import os
import sys
import copy
import time
import pickle
from joblib import dump
import argparse
from importlib import import_module
import numpy as np
from tqdm import tqdm
from skopt import Optimizer
from multiprocessing import Pool


# Define objective
def objective(variable_params, params, exp_dir, gpu_id='', save_figs=True,
              verbose=True):

    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_id

    # Translate from hyperparameter space to dict passed to ModelContainer

    # First merge the dicts, such that all entries (keys) in params
    # overlapping with entries in the search space (variable_params) are overwritten
    params = merge_dicts(params, variable_params)

    # This is where custom search parameters are integrated into params.
    # For instance, maybe you want to sample the mini-batch size in powers of 2,
    # by way of an integer parameter in the search space called 'log2_batch_size'.
    # This could then the translated into something ModelContainer will understand
    # as follows:
    if 'log2_batch_size' in params:
        params['batch_size'] = 2 ** params['log2_batch_size']

    # Set learning rate based on optimizer and batch size
    if params['auto_lr']:
        if isinstance(params['optimizer'], dict):
            optimizer_name = params['optimizer']['class_name']
        else:
            optimizer_name = params['optimizer']
        lr, lr_schedule_range = get_auto_lr(optimizer_name, params['batch_size'])
        if params['lr_schedule']['name'] is not None:
            params['lr_schedule']['range'] = lr_schedule_range
            if verbose:
                print('Learning rate range for learning rate schedule '
                      f'{params["lr_schedule"]["name"]} set to '
                      f'({params["lr_schedule"]["range"][0]:.5e}, '
                      f'{params["lr_schedule"]["range"][1]:.5e}) '
                      'by auto_lr.')
        else:
            if isinstance(params['optimizer'], dict):
                params['optimizer']['config']['lr'] = lr
            else:
                params['optimizer'] = {'class_name':params['optimizer'],
                                       'config':{'lr':lr}}
            if verbose:
                print('Learning rate has been set to  '
                      f'{params["optimizer"]["config"]["lr"]:.5e} by auto_lr.')

    # It is currently not supported to use several GPUs from within a single process in the pool
    params['n_gpus'] = len(gpu_id.replace(',',''))

    # Make directories for saving figures and models
    dirs = create_directories(exp_dir, params['epochs'], log_prefix=gpu_id) # returns dictionary

    # Redirect output
    sys.stdout = open(dirs['log'] + 'std.out', 'w')
    sys.stderr = open(dirs['log'] + 'std_error.out', 'w')

    # If a datagenerator is used, load a single point for each set (e.g. train, val
    # and test) to construct the model with (the construction uses the shapes of the
    # data).
    if params['use_datagenerator']:
        n_points_original = copy.deepcopy(params['n_points'])
        params['n_points'] = {set_name:1 for set_name in params['n_points']}

    # Load data
    data = load_data(data_path, n_points=params['n_points'],
                     img_names=params['img_names'],
                     scalar_names=params['scalar_names'],
                     eval_scalar_names=params['eval_scalar_names'],
                     load_sample_weights=params['use_sample_weights'],
                     load_time_imgs=params['use_times'],
                     load_tracks=params['use_tracks'],
                     verbose=verbose*(not params['use_datagenerator']))

    # Instantiate model container (with self.model in it)
    mc = ModelContainer(data=data,
                        params=params,
                        dirs=dirs,
                        save_figs=save_figs,
                        verbose=verbose)

    # Revert back to the original n_points
    if params['use_datagenerator']:
        mc.params['n_points'] = n_points_original

    # Save hyperparameters
    params_for_saving = copy.deepcopy(mc.params)
    params_for_saving['n_params'] = mc.model.count_params()
    save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

    # Train model
    mc.train()

    # Evaluate (predicting on test set and plotting)
    if not hasattr(mc,'evaluation_scores'):
        mc.evaluate()

    # Print results
    if verbose:
        print('Evaluation scores:')
        print(mc.evaluation_scores, flush=True)

    # Use first entry in metrics list as evaluation function used by the
    # Gaussian process hyperparameter search
    score = mc.evaluation_scores[mc.model.metrics_names[-1]]

    return point_aslist(search_space_as_dict, variable_params), score



if __name__=='__main__':

    # Fixes relative imports
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    sys.path.append(BASE_DIR)
    sys.path.append(os.path.join(BASE_DIR, '../'))

    # Own imports
    from model_container import ModelContainer
    from model_building_functions import get_loss_function, get_auto_lr
    from utils import load_data, create_directories, save_dict, merge_dicts, point_asdict, point_aslist, str2bool

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
    parser.add_argument('--n_calls', help='How many networks should be trained for the chosen search space.', default=int(100), type=int)
    parser.add_argument('--n_initial_points', help='How many random initial points the Gaussian process should spawn before trying to navigate the landscape based on the acquisition function.', default=int(10), type=int)
    parser.add_argument('--n_jobs', help='Number of cores to run in parallel while running the acquisition function. Set to -1 for all.', default=int(4), type=int)
    parser.add_argument('--n_datagen_workers', help='Number of CPU workers to use in datageneration, if chosen.', default=int(1), type=int)
    parser.add_argument('--max_queue_size', help='Passed to datagenerator, if used.', default=int(10), type=int)
    parser.add_argument('--data_path', help='Path to data. Can be relative.', required=True, type=str)
    parser.add_argument('--exp_name', help='Name of experiment folder, e.g. "my_exp".', required=True, type=str)
    parser.add_argument('-s','--save_figs', help='Save figures.', default=True, type=str2bool)
    parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
    args = parser.parse_args()

    gpu_ids = args.gpu
    n_calls = args.n_calls
    n_initial_points = args.n_initial_points
    n_jobs = args.n_jobs
    n_datagen_workers = args.n_datagen_workers
    max_queue_size = args.max_queue_size
    data_path = args.data_path
    exp_name = args.exp_name
    exp_dir = './' + exp_name + '/'
    save_figs = args.save_figs
    verbose = args.verbose

    # Import parameter configurations as a module
    param_conf = import_module(exp_name + '.param_conf')

    # Set which GPU(s) to use
    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

    # Get hyperparameters
    params = param_conf.get_params()
    params['data_path'] = data_path
    params['n_datagen_workers'] = n_datagen_workers
    params['max_queue_size'] = max_queue_size
    search_space = param_conf.get_search_space()

    # Load data (if a data generator is not used)
    if not params['use_datagenerator']:
        data = load_data(data_path, n_points=params['n_points'],
                         img_names=params['img_names'],
                         scalar_names=params['scalar_names'],
                         eval_scalar_names=params['eval_scalar_names'],
                         load_sample_weights=params['use_sample_weights'],
                         load_time_imgs=params['use_times'],
                         load_tracks=params['use_tracks'],
                         verbose=verbose)

    # Save search space
    search_space_as_dict = {var.name:var.bounds for var in search_space}
    search_space_for_saving = merge_dicts(params, search_space_as_dict)
    save_dict(search_space_for_saving, exp_dir + 'param_search_space.txt', overwrite=True, save_pkl=True)

    # Prepare for parallelization over the chosen GPUs
    gpu_id_list = gpu_ids.split(',')

    # For each GPU, keep track of its results, which has a ready() method,
    # telling if it has completed its given task
    gpu_state = {id:None for id in gpu_id_list}

    # Initialize optimizer, which will keep track of the results received from
    # all GPUs
    opt = Optimizer(search_space, # TODO: Add noise
                    n_initial_points=n_initial_points,
                    acq_optimizer_kwargs={'n_jobs':n_jobs})

    # Begin the process of as many workers as there are visible GPUs
    with Pool(processes=len(gpu_id_list)) as pool:

        n_completed_calls = 0
        ready_ids = [id for id in gpu_state]

        pbar = tqdm(total=n_calls)

        while n_completed_calls < n_calls:

            if len(ready_ids) > 0:

                # Update optimizer state
                n_updated = 0
                for id in ready_ids:
                    if gpu_state[id] is not None:
                        result = gpu_state[id].get()
                        if verbose:
                            if id != '':
                                pbar.write(f'GPU {id} returned {result[1]}.')
                            else:
                                pbar.write(f'Returned {result[1]}.')
                        opt_result = opt.tell(*result)
                        n_updated += 1
                n_completed_calls += n_updated
                if verbose:
                    pbar.update(n_updated)

                # Save optimizer state and most recent results
                if n_updated > 0:
                    with open(exp_dir + 'optimizer.pkl', 'wb') as f:
                        pickle.dump(opt, f)
                    with open(exp_dir + 'results.pkl', 'wb') as f:
                        # Delete objective function before saving. To be used when results have been loaded,
                        # the objective function must then be imported from this script.
                        if opt_result.specs is not None:
                            if 'func' in opt_result.specs['args']:
                                res_without_func = copy.deepcopy(opt_result)
                                del res_without_func.specs['args']['func']
                                dump(res_without_func, f)
                            else:
                                dump(opt_result, f)
                        else:
                            dump(opt_result, f)

                # Sample points for all idle GPUs
                sampled_points = opt.ask(len(ready_ids))

                # Convert sampled points to dict
                sampled_points = [point_asdict(search_space_as_dict, point) for point in sampled_points]

                # Distribute and evaluate the sampled points over the workers
                for point,id in zip(sampled_points,ready_ids):
                    gpu_state[id] = pool.apply_async(objective, args=(point,params,exp_dir,id,save_figs,verbose))

                # Check if there are any ready (idle) GPUs
                ready_ids = [id for id in gpu_state if gpu_state[id].ready()]

            else:
                # Wait a bit, so the while-condition isn't checked constantly
                time.sleep(10)

                # Check if there are any ready (idle) GPUs
                ready_ids = [id for id in gpu_state if gpu_state[id].ready()]

        pbar.close()
