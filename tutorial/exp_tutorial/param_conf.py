def get_params():
    """
    Returns a dictionary containing all parameters to be passed to the model
    container.

    Please see the README for documentation.
    """

    params = {

          # Training
          'epochs'                     : 10,
          'batch_size'                 : 256,
          'loss'                       : 'categorical_crossentropy',
          'metrics'                    : ['categorical_accuracy'],
          'optimizer'                  : {'class_name':'Nadam', 'config':{'lr':4e-4}}, # You can also simply write 'Nadam' to use the default learning rate
          'lr_finder'                  : {'use':False,
                                          'scan_range':[1e-4, 1e-2],
                                          'epochs':1,
                                          'prompt_for_input':False},
          'lr_schedule'                : {'name':None},
          'auto_lr'                    : False,
          'use_earlystopping'          : False,
          'restore_best_weights'       : True,
          'pretrained_model'           : {'use':False},

          # Data (don't worry about these for now)
          'n_points'                   : {'train':None, 'val':None, 'test':None},
          'use_datagenerator'          : False,
          'use_sample_weights'         : False,
          'img_names'		           : ['img'],
          'scalar_names'               : None,
          'eval_scalar_names'          : None,
          'use_times'                  : False,
          'use_tracks'                 : False,
          'upsampling'                 : {'use':False,
                                          'wanted_size':(56,55),
                                          'interpolation':'nearest'},

          # Submodels
          'top'                        : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[32,32,10],
                                          'final_activation':'softmax'},
          'cnn'                        : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'cnn_type':'simple',
                                          'conv_dim':2,
                                          'block_depths':[1,2,2,2],
                                          'n_init_filters':32,
                                          'init_kernel_size':5,
                                          'rest_kernel_size':3,
                                          'globalavgpool':False,
                                          'downsampling':'maxpool',
                                          'min_size_for_downsampling':7},
          'FiLM_gen'                   : {'use':False} # This is only to be used with scalar variables
          }

    return params
