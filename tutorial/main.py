import os
import sys
import copy
import argparse
import numpy as np
import keras as ks
from keras.datasets import mnist

# Fixes relative imports
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/..')

# Own imports
from model_container import ModelContainer
from utils import create_directories, save_dict, str2bool

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
parser.add_argument('--exp_name', help='Name of experiment folder, e.g. "my_exp".', required=True, type=str)
parser.add_argument('-s','--save_figs', help='Save figures.', default=True, type=str2bool)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=1, type=int)
args = parser.parse_args()

gpu_ids = args.gpu
exp_name = args.exp_name
exp_dir = './' + exp_name + '/'
save_figs = args.save_figs
verbose = args.verbose

# Import parameter configurations as a module
from importlib import import_module
param_conf = import_module(exp_name + '.param_conf')

# Set which GPU(s) to use
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

def get_data():
    """
    A function made so that it is easy to see what part of the code is handling
    the data and its preparation, which is very specific to this tutorial.
    """

    # Load example data (here MNIST)
    # The data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    # Split test into test and val
    x_test, x_val = np.array_split(x_test,2)
    y_test, y_val = np.array_split(y_test,2)

    # Organize into dictionary
    # This is the data structure that is expected by the ModelContainer class seen below.
    # The image name 'img' should correspond to the 'img_names' in param_conf.py.
    # The model constructed by the ModelContainer instance will automatically adapt
    # to the input dimensions of the data that is fed to it.
    data = {'train':{'img':x_train, 'Y':y_train},
            'val':{'img':x_val, 'Y':y_val},
            'test':{'img':x_test, 'Y':y_test}}

    # Prepare the images
    img_rows, img_cols = 28, 28

    for set_name in data:
        data[set_name]['img'] = data[set_name]['img'].reshape(data[set_name]['img'].shape[0], img_rows, img_cols, 1)
        data[set_name]['img'] = data[set_name]['img'].astype('float32')
        data[set_name]['Y'] = ks.utils.to_categorical(data[set_name]['Y'])
        data[set_name]['img'] /= 255

    return data

# Get the data for this tutorial
data = get_data()

# Get hyperparameters
params = param_conf.get_params()
params['n_gpus'] = len(gpu_ids.replace(',',''))

# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = create_directories(exp_dir, params['epochs'], log_prefix=gpu_ids)

# Instantiate model container (with self.model in it)
# This is where all the magic happens - have a look in model_container.py to get an idea of what is going on
mc = ModelContainer(data=data,
                    params=params,
                    dirs=dirs,
                    save_figs=save_figs,
                    verbose=verbose)

# Save hyperparameters
params_for_saving = copy.deepcopy(mc.params)
params_for_saving['n_params'] = mc.model.count_params() # inlcude number of model parameters
save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

# Train model
mc.train()

# Evaluate (predicting on test set and plotting)
if not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results
if verbose:
    print('Evaluation scores:')
    print(mc.evaluation_scores)
