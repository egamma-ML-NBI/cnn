# This repository has moved to https://gitlab.com/ffaye/deepcalo, which can be installed through 
```bash
pip install deepcalo
```

## ATLAS-CNN
### Python 3 Keras CNN implementation for energy regression and particle identification
Author: Frederik Faye, The Niels Bohr Institute, 2019

## What is ATLAS-CNN?
This repository contains code for building, training and tuning various convolutional neural network (CNN) models using Keras, using any backend.
The code is written to be modular and to provide extensive, but easy to digest logging.

The models that can be built have been designed specifically with the ATLAS detector in mind, but you can also just use the framework and all its nice features for any Keras-based project.

Note that ATLAS-CNN is not meant to be used as a Python package, but rather as something to be forked and, if need be, tailored to your specific project.

## Why should you use it?
#### It is good at organizing your experiments
Doing deep learning involves a lot of experimentation, and it is therefore paramount to organize your results in smart way. This repository does this by creating a time-stamped folder for each run, which contains hyperparameters, plots of model architectures, the weights of the model during training, and more.
These folders are further organized into "experiment directories", containing runs investigating similar things.

#### It makes it easy to try out a lot of things
Just by setting the parameters of a Python dictionary, you can create a large variety of models: You can for example control the depth of your CNN, the kernel sizes used in it, whether to use downsampling, which activation functions to use, regularization, and much more.
In the same dictionary, you can also set parameters important for training, e.g., the batch size or your optimizer  of choice.

A lot of more advanced features are also supported. To name a few: Cyclic learning rate schedules, a learning rate finder, 3D convolutions, residual architectures, and non-standard optimizers such as Yogi and Padam. You can also easily use parts of pretrained networks. See the [documentation](#docs) for detailed information about each parameter.

#### It is easily extensible
Most of the core functionality of this framework is placed in the two scripts `model_container.py` and `model_building_functions.py`. These are written to be as modular as possible, which makes it easy to add your own designs.
For instance:
* To use your own CNN architecture, just make the `get_cnn` function in `model_building_functions.py` return it as a Keras model, and it will be integrated into the rest of the framework.
* Or, say you want to use a certain callback. Then simply add it to the list in `get_callbacks` in `model_container.py`.

If you are unsure how to implement something, don't hesitate to open an issue and ask!

#### It is designed with hyperparameter optimization in mind
All of the above was designed to make hyperparameter optimization easy. ATLAS-CNN uses scikit-optimize's Bayesian optimization for hyperparameter searches, and is set up to use multiple GPUs.

### Table of Content
- [Dependencies](#dependencies)
- [Usage](#usage)
    - [Quick start](#quickstart)
    - [Running an experiment](#runningexp)
- [Model architectures](#archs)
    - [CNN](#arch_cnn)
    - [Top](#arch_top)
    - [Scalar net](#arch_scalar_net)
    - [FiLM generator](#arch_film_gen)
    - [Track net](#arch_track_net)
    - [Time net](#arch_time_net)
    - [Combined](#arch_combined)
- [Documentation for `param_conf`](#docs)
    - [Keys concerning training](#doc_training)
    - [Keys concerning data](#doc_data)
    - [Keys concerning all submodels](#doc_allsubmodels)
    - [Keys concerning submodel `top`](#doc_top)
    - [Keys concerning submodel `cnn`](#doc_cnn)
    - [Keys concerning submodel `scalar_net`](#doc_scalar_net)
    - [Keys concerning submodel `FiLM_gen`](#doc_film_gen)
    - [Keys concerning submodel `track_net`](#doc_track_net)
    - [Keys concerning submodel `time_net`](#doc_time_net)
- [Known issues](#knownissues)
- [Todo](#todo)

<a name="dependencies"></a>
## Dependencies
numpy, pandas, matplotlib, h5py, joblib, tqdm, keras, tensorflow, pydot, graphviz, scikit-optimize, keras_contrib, keras_drop_block

<a name="usage"></a>
## Usage
<a name="quickstart"></a>
### Quick start
Clone the repository and `cd` to the folder containing it. Now, assuming you are using conda, set up a virtual environment and run the tutorial:
```bash
conda env create -f environment.yml; conda activate atlas_cnn
cd tutorial
python main.py --exp_name exp_tutorial
```
#### In words:
We first create the virtual environment as specified by the provided `enironment.yml` file. Here, we will use TensorFlow as the backend for Keras. If you want to use another backend, please install that instead and set e.g. `os.environ['KERAS_BACKEND'] = 'theano'` right before importing Keras in `model_container.py` (note that you must have TensorFlow installed to use TensorBoard).

Then we go to the directory *tutorial*. Here, an experiment is set up by way of the already created *exp_tutorial* folder. We use the Python script `main.py` to create a model based on the parameters given by the `param_conf.py` script in the experiment directory (here *exp_tutorial*).
Read through `main.py` to see how this is done without getting your hands too dirty.

Running `main.py` will train a small CNN to discriminate between the digits of the MNIST dataset, which should achieve $`\sim98\%`$ test accuracy within its first epoch. Try playing around with the parameters in `param_conf.py` to see if you can find a network that does better! Also have a look at the contents of the *logs* folder in the experiment directory to see some of the nice logging features this framework has to offer.

Some of the more advanced options, like using a learning rate schedule, have been omitted from the tutorial; see the [documentation](#docs) for what is possible (see the `param_conf.py` in *run_model/template/* for an example).

<a name="runningexp"></a>
### Running an experiment
This repository is split into two parts: One for working with a model with a specific set of hyperparameters (the *run_model* folder), and one for searching over a space of hyperparameters (the *hp_search* folder).

#### Setting up an experiment
For both of the above cases, the best way to keep track of ones experiments is to make a new folder in either *run_model* or *hp_search*, say *exp0*. This folder should contain an `__init__.py`, as well as a `param_conf.py`. The latter specifies the parameters given to the model, and - in the case of *hp_search* - also specifies the search space. See the *template* folders in *run_model* and *hp_search* for examples. Note that if you are using the `.gitignore` from this repository, all directories in either *run_model* or *hp_search* beginning with *exp* will be ignored by git.

#### Running an experiment
After having set the desired parameters in `param_conf.py`, run `main.py` in the parent folder.

In the case of *run_model*, a KeyboardInterrupt during training will prompt a question asking if prediction on the test should be carried out.

Note that the `--data_path` (the path of the data) and `--exp_name` (the name of the experiment directory, e.g. *exp0*) flags are required.
For example:
```bash
python main.py --exp_name exp0 --data_path ~/Desktop/my_data.h5 -g 0
```

#### Loading data
Right now, the `load_data` function of `utils.py` is tailored to work with the datafiles found in the lxplus (CERNBox) directory */eos/user/l/lehrke/Data* (which is visible to all current ATLAS physicists).
If need be, you can modify this function to work with your data.

Note that this framework uses the `'channels_last'` format, which is the standard in Keras.

If your data does not fit in memory, set `use_datagenerator` to `True` in `param_conf.py`. This will load a single mini-batch at a time. If you get a CPU bottleneck, you can turn on multiprocessing (set the `n_workers` flag of `main.py` to something larger than 1) to load mini-batches using `n_workers`. You could also consider not loading your test set, and then only doing predictions at a later point, when the training and validation sets are not in memory.

#### Logs
Running `main.py` will create a time-stamped directory in *./exp_name/logs/gpu_ids/* (where *gpu_ids* could be e.g. *0_1_3*) in which hyperparameters, various figures, model weights (best so far, used for early stopping), predictions using the best model, as well as a Tensorboard log-file will be saved.
For *hp_search*, `sys.stdout` and `sys.stderr` for each worker in the pool (see below) will be redirected to the text files *std.out* and *std_error.out*, respectively, in the same time-stamped directory.

#### Parallelization
An important flag of `main.py` is `--gpu` (or `-g`) (e.g. `-g 0,1,3`), which specifies the GPU ids visible to the system when running an experiment.

In the case of *run_model*, this evenly distributes the mini-batches along the visible GPUs, which results in a quasi-linear speed-up (but remember to multiply the mini-batch size by the number of GPUs in `param_conf.py`!).

In the case of *hp_search*, a `multiprocessing.Pool` (with as many workers as there are visible GPUs) is used to handle training and evaluating one model per GPU, where each model has a set of hyperparameters sampled from the search space. When a GPU is done training a model, it tells its results to the `scikit-optimize` optimizer object and receives a new sampled set of hyperparameters. This optimizer samples the search space intelligently using previous results, by way of a Gaussian process (see the [scikit-optimize repository](https://github.com/scikit-optimize/scikit-optimize) for details), enabling searching over much larger spaces.

<a name="archs"></a>
## Model architectures
The following is a quick tour of the different out-of-the-box models available. Each model is made for a different kind of input, e.g., images, scalar variables, tracks, or the output from other models.

All models except the [top](#arch_top) are optional to use. However, models are tied to their input in such a way that if you for instance choose to `use_tracks` in `param_conf.py`, the [track net](#arch_track_net) will be integrated into the [combined model](#arch_combined).

You can find information about how to set the hyperparameters of these models in the [documentation](#docs), where each model has its own section.

<a name="arch_cnn"></a>
### CNN
Below, an illustration of the default CNN architecture can be seen. It is comprised of blocks. For all but the first block, a block begins with downsampling and the number of feature maps being doubled.

The tuple underneath the input denotes the size of the input (here height, width, channels).

Note that normalization, the activation function, downsampling and global average pooling can all be turned on or off.

The output of the CNN is passed on to the [top](#arch_top).

<p align="center">
  <img src="images/cnn_arch.png" width="350">
</p>

<a name="arch_top"></a>
### Top
The top model is a simple, dense neural network that takes as input the concatenated outputs of other models, and gives a final output, which can be any 1D size $`\geq 1`$.

<p align="center">
  <img src="images/top_arch.png" width="350">
</p>

<a name="arch_scalar_net"></a>
### Scalar net
The scalar net is again a simple, dense network that processes any scalar variables you may want to include. Its output can be connected to either the [top](#arch_top), the [FiLM generator](#arch_film_gen), or both.

<p align="center">
  <img src="images/scalar_net_arch.png" width="350">
</p>

<a name="arch_film_gen"></a>
### FiLM generator
The FiLM generator is a nice way of conditioning the CNN with scalar variables. You can read a good introduction to the technique [here](https://distill.pub/2018/feature-wise-transformations/).

The FiLM generator can take inputs from both the [scalar net](#arch_scalar_net) and the [track net](#arch_track_net). Its output modulates the [CNN](#arch_cnn).

<p align="center">
  <img src="images/film_gen_arch.png" width="350">
</p>

<a name="arch_track_net"></a>
### Track net [WIP]
NOTE: This architecture has not yet been tested.

This model takes the (varying) number of track vectors for a datapoint as input and spits out a fixed size representation of that datapoint, which is then passed on to the [top](#arch_top), the [FiLM generator](#arch_film_gen), or both.

As the order in which we give our model the track vectors for a datapoint carries no information, the permutation invariant method of [Deep Sets](https://arxiv.org/abs/1703.06114) has been used.

The $`T`$ in the shape of $`X_{\mathrm{track}}`$ is the largest number of track vectors of any datapoint in the dataset, where zero-padding has been used if the actual number of tracks for a given datapoint is smaller than $`T`$.

Note that right now, the aggregation part of the track net is a simple sum, as in the [Deep Sets](https://arxiv.org/abs/1703.06114) paper.

<p align="center">
  <img src="images/track_net_arch.png" width="350">
</p>

<a name="arch_time_net"></a>
### Time net
The [CNN](#arch_cnn) architecture can be expanded to include information about when in time each cell determined its signal to be, in order to help mitigate out-of-time pileup.

The time for each cell in each channel (typically corresponding to a layer in the calorimeter) is collected in an image tensor $`X_{\mathrm{time-img}}`$ of the same resolution and dimension as the standard cell image tensor $`X_{\mathrm{img}}`$.
$`X_{\mathrm{time-img}}`$ is first passed through a gating mechanism (the time net), which outputs a real number between zero and one for each pixel in each channel. These numbers are then merged with $`X_{\mathrm{img}}`$, either by element-wise multiplication and then concatenation along the channel axis, or just by element-wise multiplication. The idea is that the element-wise multiplication allows the network to tone down the values of out-of-time cells.

The resulting, merged tensor is then given as the input to the [CNN](#arch_cnn) (in the stead of $`X_{\mathrm{img}}`$).

It is recommended to use `pgauss_f` as the [final activation](#doc_film_gen) in the time net.

<p align="center">
  <img src="images/time_arch.png" width="450">
</p>

<a name="arch_combined"></a>
### Combined
In the final illustration below, you can see how the models all fit together.

<p align="center">
  <img src="images/combined_arch.png" width="600">
</p>

#### Good hyperparameter configurations
See https://indico.cern.ch/event/800614/contributions/3327152/attachments/1799540/2936007/presentation_cnn.pdf for some slightly outdated results on energy regression using the above described networks with good hyperparameters.

<a name="docs"></a>
## Documentation for `param_conf`
Every experiment directory needs a script `param_conf.py` containing the function `get_params`, which returns a dictionary of parameters to be used in that experiment.
The following is the documentation for this dictionary, explaining what can be done and how.

```python
def get_params():
    """
    Returns a dictionary containing all parameters to be passed to the model
    container.
    """
```
When a dictionary key is referenced below, what is actually meant is the
value corresponding to that key. For instance, although the key `'epochs'`
is of course a *str*, the documentation below concerns itself with the value
of this key, which in this case is an *int*.

<a name="doc_training"></a>
### Keys concerning training:
**epochs** : *int*
- Number of epochs to train for. If `use_earlystopping` is set to
`True`, training may stop prior to completing the chosen number of
epochs.

**batch_size** : *int*
- Mini-batch size. Note that if several GPUs are used, the mini-batch
  will be evenly divided among them.

**loss** : *str*
- Any Keras loss function name. See `get_loss_function` in
`model_building_functions.py` for implemented custom loss functions,
as well as how to implement your own.

**metrics** : *list of strs or None*
- Can be the name of any metric recognized by Keras. This (or these)
metric(s) will be shown during training, as well as in the final
evaluation.
Note that if a *hp_search* is carried out, the last entry of the list
will be the evaluation function used by the Gaussian process
hyperparameter search. If an empty list or `None`, the `loss` will be
the evaluation function used by the Gaussian process hyperparameter search.

**optimizer** : *str or config dict*
- Which optimizer to use. Any Keras optimizer, as well as the Padam
and Yogi optimizers from keras_contrib, can be used.

  If you don't want to simply use the defaults parameters of the
  chosen optimizer, instead give a *config dict*. See [Explanation of
  *str or config dict*](#doc_config_dict).

**lr_finder** : *dict*
- Dictionary of learning rate finder (from [Smith, 2015](https://arxiv.org/abs/1506.01186)) parameters.
The `'use'` key is a *bool* deciding whether or not to use the
learning rate finder as implemented in `custom_classes.py`.
The `'scan_range'` is a *list* containing the minimum and maximum
learning rate to be scanned over.
The `'epochs'` key is an *int* setting the number of epochs to use in the scan.
1-4 epochs is typically enough, depending on the size of the training set.
If `'prompt_for_input'` is `True`, the user will be asked to input a
range within which the cyclical learning rate schedule (see below)
should vary in between upon completing the learning rate finder
scan.

**lr_schedule** : *dict*
- Dictionary of learning rate schedule parameters.
The `'name'` key can be either `None` (when no learning rate schedule
will be used), `'CLR'` (when the triangular schedule from [Smith, 2015](https://arxiv.org/abs/1506.01186) will be used) or `'SGDR'` (when the SGD with warm restarts, by
[Loshchilov and Hutter, 2017](https://arxiv.org/abs/1608.03983) will be used.)
]Besides a 'name', keyword arguments can be passed to the callback
chosen, e.g. `{'name':'CLR', **kwargs}`, where
`kwargs = {'range':[1e-3,5e-3], 'step_size_factor':5}`.

**auto_lr** : *bool*
- Whether to use the function `get_auto_lr` in
`model_building_functions.py` that automatically sets a good learning
rate based on the chosen optimizer and the batch size, taking the
learning rate to be propertional to the square root of the batch
size. The constant of proportionality varies from optimizer to
optimizer, and probably from problem to problem - use the learning
rate finder to find out which constant is suitable for your problem.

**use_earlystopping** : *bool*
- Use the Keras EarlyStopping callback with `min_delta=0.001` and
`patience=150` (these can be changed in `model_container.py`).

**restore_best_weights** : *bool*
- Restore the best weights found during training before evaluating.

**pretrained_model** : *dict*
- Dictionary of parameters for using (parts of) pretrained models.
The `'use'` key is a boolean deciding whether or not to load
pretrained weights. The `'weights_path'` is the path to the weights
of the pretrained network. If `'params_path'` is `None`, the
parameters for the pretrained network is assumed to be in the parent
folder of the `'weights_path'`.
`'layers_to_load'` is a *list* with the Keras names of the layers (or
submodels) whose weights should be transferred from the pretrained
model to the one at hand. These names must refer to the same
structure in both the pretrained model and in the model at hand.
`'freeze_loaded_layers'` can be either a boolean (when, if `True`,
all layers listed in `'layers_to_load'` will be frozen, or not, if
`False`) or a *list* of *bool*s with the same length as
`'layers_to_load'` (when the first boolean in
`'freeze_loaded_layers'` answers whether to freeze the first layer
given by `'layers_to_load'` or not, etc.).

<a name="doc_data"></a>
### Keys concerning data:
**n_points** : *dict*
- This dictionary defines the names of the subsets of the data loaded
(by way of the keys of `n_points`), as well as how many datapoints
should be loaded for each set (by way of the values of `n_points`).
This information is passed to `load_data` in `utils.py` through
`main.py`.

  Its *keys* should be either `'train'` and `'val'`, or `'train'`,
  `'val'` and `'test'`. This means that while both training and
  validation data are required, test data can be left out. If test
  data is left out, evaluation at the end of training will be carried
  out using the `'val'` set.
  It is important that the naming conventions of `'train'`, `'val'`
  and `'test'` are kept, as this is the only way the subsequent
  handling of the data knows which sets are meant to be used for what.

  The *values* of `n_points` dictate how many points of each set will
  be loaded. If `None`, all datapoints in the set corresponding to
  the key will be loaded. If instead a number n (*int* or *float*,
  where the latter will be converted into an *int*), only the first n
  datapoints of that set will be loaded. If a *list* of *int*s, this
  will be interpreted as indices, and only datapoints with these
  indices will be loaded (primarily used in conjunction with data
  generators).

**use_datagenerator** : *bool*
- Whether to use a data generator to load data in batches. Useful if
your dataset does not fit in memory.
**use_sample_weights** : *bool*
- Whether to load sample weights to be used in training. Useful if
classes are unbalanced or if certain marginal signal/background
distributions do not follow each other nicely.

**img_names** : *list of strs or None*
- List of image names to be passed to `load_data` in `utils.py` -
change what happens therein to make it recognize the `img_names` of
your choice.
Only if not `None` will images be loaded and a [CNN](#arch_cnn) be constructed and used.

**scalar_names** : *list of strs or *None*
- List of scalar variable names (meant to be used in training) to be
passed to `load_data` in `utils.py` - change what happens therein to
make it recognize the `scalar_names` of your choice.
Only if not `None` will scalar variables be loaded and a [scalar net](#arch_scalar_net) be constructed and used.

**eval_scalar_names** : *list of strs or *None*
- List of scalar variable names (meant to be used in evaluation) to be
passed to `load_data` in `utils.py` - change what happens therein to
make it recognize the `eval_scalar_names` of your choice.
If `None`, no evaluation scalar variables will be loaded.

**use_times** : *bool*
- Whether to load time images. See `load_data` in `utils.py` for more
details.
Only if not `None` will time images be loaded and a [time net](#arch_time_net) be constructed and used.

**use_tracks** : *bool*
- Whether to load tracks. See `load_data` in `utils.py` for more
details.
Only if not `None` will tracks be loaded and a [track net](#arch_track_net) be constructed and used.

**usampling** : *dict*
- Dictionary of parameters for upsampling input images inside the
network. This can be useful if the ability to downsample (which
introduces translational invariance) is important but the input
images are small.
The `'use'` key is a boolean deciding whether to upsample or not.
`'wanted_size'` refers to the size that all images should be
upsampled to before being concatenated. The `'interpolation'`
argument is passed to the Keras layer `UpSample2D`.

  After upsampling, the cell image tensor is normalized so as to
  maintain the same amount of energy overall, but now spread out over
  the upsampling pixels.

<a name="doc_allsubmodels"></a>
### Keys concerning all submodels:
**initialization** : *str or config dict*
- Initialization of the parameters of the submodel. Can be any
initializer recognized by Keras.

  If you don't want to simply use the defaults parameters of the
  chosen initializer, instead give a *config dict*. See [Explanation
  of *str or config dict*](#doc_config_dict).

**normalization** : *str or config dict or None*
- Normalization layer. If not `None`, the chosen normalization layer is
placed after every dense or convolutional layer in the submodel,
i.e., before an activation function.

  Can be any of `'batch'`, `'layer'`, `'instance'` or `'group'`.
  Note that the last three are implemented through a group
  normalization layer (which encompass the layer and instance
  normalization). This means that the name of the normalization
  layer when using `keras.utils.plot_model` will be the name of a
  group normalization layer when using any of the last three.

  If you don't want to simply use the defaults parameters of the
  chosen normalization layer, instead give a *config dict*. See
  [Explanation of *str or config dict*](#doc_config_dict).

**activation** : *str or config dict or None*
- Activation function of all dense or convolutional layers in the
submodel, except for the very last one, if a `final_activation`
variable is present.

  Can be any of `'relu'`, `'leakyrelu'`, `'prelu'`, `'elu'` or
  `'swish'`.
  See `get_activation` in `model_building_functions.py` for examples
  of implementations of custom activation functions.

  Is placed right after every normalization layer
  in the submodel, or - if `normalization` is `None`, right after
  every dense or convolutional layer in the submodel.

  If you don't want to simply use the defaults parameters of the
  chosen activation, instead give a *config dict*. See [Explanation of
  *str or config dict*](#doc_config_dict).

**layer_reg** : *dict with None or strs or config dicts as values*
- Layer regularization to be applied to all dense or convolutional
layers in the submodel. This dict collects `kernel_regularizer`s,
`bias_regularizer`s, `activity_regularizer`s, `kernel_constraint`s
and `bias_constraint`s to be applied.

  If you don't want to simply use the defaults parameters of the
  chosen regularizer, instead give a *config dict*. See [Explanation
  of *str or config dict*](#doc_config_dict).

  An example of what is allowed:
  ```python
  {'kernel_regularizer':'l2',
  'bias_regularizer':{'class_name':'l1',
                      'config':{'l':1e-5}},
  'activity_regularizer':None,
  'kernel_constraint':{'class_name':'max_norm',
                       'config':{'max_value':3}},
  'bias_constraint':'max_norm'}
  ```

  Any of these keys can be left out to invoke the default value of
  `None`. If the *dict* is empty, no layer regularization will be
  applied.

**dropout** : *float or dict or None*
- Arguments to be passed to either dropout layers, in the case of
dense layers (pass the rate as a float), or dropblock layers, in the
case of convolutional layers (pass a dict to be unpacked into the
dropblock layers - see the [dropblock documentation](https://github.com/MLearing/Keras-DropBlock/blob/master/keras_drop_block/drop_block.py) for what to pass).
The dropout or dropblock layers will be inserted immediately after
each dense or convolutional layer.
If `None`, no dropout or dropblock layers will be added.

<a name="doc_top"></a>
### Keys concerning submodel `top`:
Submodel for collecting inputs (e.g. from other submodels) and giving
the output of the full model.

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**units** : *list of ints*
- List with the number of hidden units in each dense layer in the top
as elements.
This includes the output neuron(s), so the last element in `units`
should be the number of desired outputs.

**final_activation** : *str*
- Activation function to apply to the last dense layer. E.g. for
binary classification, use `'sigmoid'`, and use `'linear'` or `None`
(or `'relu'` to enforce non-negativity) for regression.

<a name="doc_cnn"></a>
### Keys concerning submodel `cnn`:
Submodel for processing images. Will only be used if `img_names` is not None.

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**cnn_type** : *str*
- The type of CNN that will be constructed. To use the CNN illustrated
[here](#arch_cnn), set to `'simple'`. Set to `'res'` to use residual
blocks, as in [He et al., 2016](https://arxiv.org/abs/1603.05027).
Setting cnn_type to some other string is a good way to implement
other types of CNNs, which are then integrated into the framework.
For instance, to use the ResNet18 of keras_contrib, set to `'res18'` - see `model_building_functions.py` under `get_cnn` for how this is
done.

**conv_dim** : *int*
- One of `2` or `3`. Whether to use 2D or 3D convolutions.

**block_depths** : *list* of *int*s
- List with number of convolutional layers for each block as elements.
See the illustration [here](#arch_cnn) for what constitutes a block.

  Note that is `cnn_type` is `'res'`, two convolutional layers are
  used per *int*, e.g. a `block_depth` value of `[1,2,2,2,2]` will
  result in a CNN with 18 convolutional layers, wheres the CNN would
  only have 9 convolutional layers if `cnn_type` had been `'simple'`.

**n_init_filters** : *int*
- How many filters should be used in the first convolutional layer.

**init_kernel_size** : *int* or *tuple*
- Kernel size of the first convolutional layer.

  If and *int* is given and `conv_dim` is `2`, a kernel size of
  `(init_kernel_size,init_kernel_size)` is used. If `conv_dim` is
  instead `3`, a kernel size of `(init_kernel_size,init_kernel_size,2)`
  is used.
  If a tuple is given, its length must equal `conv_dim`.

**rest_kernel_size** : *int or tuple*
- Kernel size of all but the first convolutional layer.

  If and *int* is given and `conv_dim` is `2`, a kernel size of
  `(init_kernel_size,init_kernel_size)` is used. If `conv_dim` is
  instead `3`, a kernel size of `(init_kernel_size,init_kernel_size,2)`
  is used.
  If a tuple is given, its length must equal `conv_dim`.

**globalavgpool** : *bool*
- Whether to use global average pooling in the end of the CNN.

**downsampling** : *str or None*
- One of `None` (no downsampling is used), `'avgpool'` (with
`pool_size=2`), `'maxpool'` (with `pool_size=2`), or `'strided'`
(when strided convolutions with stride and kernel size of 2 is used
to downsample).

  When one dimension is more than 1.5 times larger than another
  dimension, that (larger) dimension will be downsampling such that it
  is reduced by a factor of 3, instead of 2. This can be changed in
  `get_downsampling` in `model_building_functions.py`.

**min_size_for_downsampling** : *int*
- The minimum that any dimension over which convolutions are made (so
excluding samples and channels dimensions) must be if downsampling
is to take place. This is to prevent downsampling down to too small
images.

  E.g., if 2D downsampling is attempted on a `(None,7,6,4)` image
  tensor while `min_size_for_downsampling` is `6`, the downsampling
  goes through and the result would be `(None,3,3,4)`. If, on the
  other hand, `min_size_for_downsampling` was `7`, the third dimension
  of the image tensor would be too small, and no downsampling would
  take place.

<a name="doc_scalar_net"></a>
### Keys concerning submodel `scalar_net`:
Submodel for processing scalar variables. Will only be used if `scalar_names` is not None.

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**units** : *list of ints*
- List with the number of hidden units in each dense layer as
elements. If empty, the input is passed on without any processing.

**connect_to** : *list of strs*
- Which other submodels should receive the output of `scalar_net` as
(part of) their input. Can contain either `'top'` and/or
`'FiLM_gen'`. It can in principle also be empty, but you should
rather turn off the use of scalar variables by setting
`scalar_names` to `None`.

<a name="doc_film_gen"></a>
### Keys concerning submodel `FiLM_gen`:
Submodel for modulating the feature maps of the `cnn` submodel, called
a FiLM generator. See [this](https://distill.pub/2018/feature-wise-transformations/) for an overview. Will only be used if the `connect_to` list of either of `scalar_net` or `track_net` contains `FiLM_gen` (and those submodels are used).

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**use** : *bool*
- Whether to use the FiLM generator.

**units** : *list of ints*
- List with the number of hidden units in each dense layer as
elements. If empty, the input is passed on without any processing.

<a name="doc_track_net"></a>
### Keys concerning submodel `track_net`:
Submodel for processing tracks. Will only be used if `use_tracks` is `True`.
Uses Deep Sets, see [Zaheer et al., 2017](https://arxiv.org/abs/1703.06114).

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**phi_units** : *list of ints*
- List with the number of hidden units in each dense layer of the phi
network (see [Zaheer et al., 2017](https://arxiv.org/abs/1703.06114)) as elements.
If empty, the input is passed on to the rho network.

**rho_units** : *list of ints*
- List with the number of hidden units in each dense layer of the rho
network (see [Zaheer et al., 2017](https://arxiv.org/abs/1703.06114)) as elements.
If empty, the input is passed on without further processing.

**connect_to** : *list of strs*
- Which other submodels should receive the output of `track_net` as
(part of) their input. Can contain either `'top'` and/or
`'FiLM_gen'`. It can in principle also be empty, but you should
rather turn off the use of tracks by setting `use_tracks` to
`False`.

<a name="doc_time_net"></a>
### Keys concerning submodel `time_net`:
Submodel for processing time images. Will only be used if `use_times` is `True`.

See "Keys concerning all submodels" for keys `initialization`,
`activation`, `normalization`, `layer_reg` and `dropout`.

**units** : *list of ints*
- List with the number of hidden units in each dense layer as
elements. If empty, the input is passed on to `final_activation`.

**use_res** : *bool*
- Whether to use a residual connection over the dense layers given by
`units`.

**final_activation** : *str or config dict*
- Activation function to apply to the last dense layer, or to the
input itself, in case `units` is empty.

  The output of the chosen activation function should be in the range
  [0;1].
  Custom activations `'gauss'`, ``'gauss_f'`, `'pgauss'` and
  `'pgauss_f'` have been implemented to use here. The "p" stands for
  "parametric", while the "f" stands for "flipped".

**final_activation_init** : *list*
- List of initial weights for parametric activation functions
'pgauss'` and `'pgauss_f'`. These contain a single parameter, namely
the width of the Gaussian, and so `final_activation_init` should
contain a single float, e.g., `final_activation_init` could be
`[0.5]`.

  If you don't want to simply use the defaults parameters of the
  chosen activation, instead give a *config dict*. See [Explanation of
  *str or config dict*](#doc_config_dict).

<a name="doc_config_dict"></a>
#### Explanation of *str or config dict*:
Say you want to use the `RandomNormal` initializer of Keras to
initialize the weights of some submodel. If you want to use the
default value of the parameters of this class (`mean=0.0, stddev=0.05,
seed=None`), you can simply give the *str* `'RandomNormal'` as the
value for the `initialization` key described above.
However, if you want to pass some other parameters to the class, you
can instead give a *config dict* as the value for the
`initialization` key. A *config dict* must have two keys,
`'class_name'` and `'config'`. The value corresponding to the
`'class_name'` key should be a *str*, e.g. `'RandomNormal'`, while the
value corresponding to the `'config'` key should be a dict containing
the keyword arguments you wish to pass to the class (an empty `'config'`
dict will use the default values).

You could for example give the following as the value corresponding to
the `initialization` key:
`{'class_name':'RandomNormal', 'config':{'stddev':1.0}}`

See the docs for `layer_reg` above for additional examples.

For a more technical definition of the *config dict*: It is what is
returned from `keras.utils.serialize_keras_object(keras_object)`
where `keras_object` is the class instance you wish to create.

In most cases, aliases are set up such that multiple names for the
same class are valid, e.g. if you want to use batch normalization as
normalization in some submodel, you can pass any of `'batch'`,
`'BatchNormalization'`, `'batch_norm'`, etc, as the `'class_name'`.

<a name="knownissues"></a>
## Known issues
* Model weights produced with `multi_gpu_model` cannot be loaded using single GPU, see https://github.com/keras-team/keras/issues/9562

<a name="todo"></a>
## Todo
* Implement a linear schedule for DropBlock, as https://github.com/miguelvr/dropblock/blob/master/dropblock/scheduler.py
