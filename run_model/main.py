import os
import sys
import copy
import time
import pickle
import argparse
import numpy as np
import keras as ks

# Fixes relative imports
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)
sys.path.append(os.path.join(BASE_DIR, '../'))

# Own imports
from model_container import ModelContainer
from model_building_functions import get_auto_lr
from utils import load_data, create_directories, save_dict, merge_dicts, str2bool

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
parser.add_argument('--data_path', help='Path to data. Can be relative.', required=True, type=str)
parser.add_argument('--exp_name', help='Name of experiment folder, e.g. "my_exp".', required=True, type=str)
parser.add_argument('--n_datagen_workers', help='Number of CPU workers to use in datageneration, if chosen.', default=int(1), type=int)
parser.add_argument('--max_queue_size', help='Passed to datagenerator, if used.', default=int(10), type=int)
parser.add_argument('--batch_size', help='Minibatch size. If an int is given, it will override the value given in param_conf.py.', default=None, type=int)
parser.add_argument('--n_train', help='How many data points to load from the training set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_val', help='How many data points to load from the validation set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_test', help='How many data points to load from the test set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--lr_finder_epochs', help='Number of epochs to do learning rate finder scan over, if chosen. Defaults to -1 to use the value given in params_conf.py.', default=int(-1), type=int)
parser.add_argument('--eval', help='Predict on test set and make evaluation plots.', default=True, type=str2bool)
parser.add_argument('--save_figs', help='Save figures.', default=True, type=str2bool)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
args = parser.parse_args()

gpu_ids = args.gpu
data_path = args.data_path
exp_name = args.exp_name
exp_dir = './' + exp_name + '/'
n_datagen_workers = args.n_datagen_workers
max_queue_size = args.max_queue_size
batch_size = args.batch_size
_n_train = int(args.n_train)
_n_val = int(args.n_val)
_n_test = int(args.n_test)
_lr_finder_epochs = args.lr_finder_epochs
eval = args.eval
save_figs = args.save_figs
verbose = args.verbose

# Import parameter configurations as a module
from importlib import import_module
param_conf = import_module(exp_name + '.param_conf')

# Set which GPU(s) to use
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

# Get hyperparameters and integrate the ones given from the command line
params = param_conf.get_params()
params['n_gpus'] = len(gpu_ids.replace(',',''))
params['data_path'] = data_path
params['n_datagen_workers'] = n_datagen_workers
params['max_queue_size'] = max_queue_size
if batch_size is not None:
    params['batch_size'] = batch_size
for set_name, n in zip(params['n_points'], [_n_train, _n_val, _n_test]):
    if not n < 0:
        params['n_points'][set_name] = n
if not _lr_finder_epochs < 0:
    params['lr_finder']['epochs'] = _lr_finder_epochs

# If a datagenerator is used, load a single point for each set (e.g. train, val
# and test) to construct the model with (the construction uses the shapes of the
# data).
if params['use_datagenerator']:
    n_points_original = copy.deepcopy(params['n_points'])
    params['n_points'] = {set_name:1 for set_name in params['n_points']}

# Load data
data = load_data(data_path, n_points=params['n_points'],
                 img_names=params['img_names'],
                 scalar_names=params['scalar_names'],
                 eval_scalar_names=params['eval_scalar_names'],
                 load_sample_weights=params['use_sample_weights'],
                 load_time_imgs=params['use_times'],
                 load_tracks=params['use_tracks'],
                 verbose=verbose*(not params['use_datagenerator']))

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    if isinstance(params['optimizer'], dict):
        optimizer_name = params['optimizer']['class_name']
    else:
        optimizer_name = params['optimizer']
    lr, lr_schedule_range = get_auto_lr(optimizer_name, params['batch_size'])
    if params['lr_schedule']['name'] is not None:
        params['lr_schedule']['range'] = lr_schedule_range
        if verbose:
            print('Learning rate range for learning rate schedule '
                  f'{params["lr_schedule"]["name"]} set to '
                  f'({params["lr_schedule"]["range"][0]:.5e}, '
                  f'{params["lr_schedule"]["range"][1]:.5e}) '
                  'by auto_lr.')
    else:
        if isinstance(params['optimizer'], dict):
            params['optimizer']['config']['lr'] = lr
        else:
            params['optimizer'] = {'class_name':params['optimizer'],
                                   'config':{'lr':lr}}
        if verbose:
            print('Learning rate has been set to  '
                  f'{params["optimizer"]["config"]["lr"]:.5e} by auto_lr.')

# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = create_directories(exp_dir, params['epochs'], log_prefix=gpu_ids)

# Instantiate model container (with self.model in it)
mc = ModelContainer(data=data,
                    params=params,
                    dirs=dirs,
                    save_figs=save_figs,
                    verbose=verbose)

# Revert back to the original n_points
if params['use_datagenerator']:
    mc.params['n_points'] = n_points_original

# Save hyperparameters (with some additional information)
params_for_saving = copy.deepcopy(mc.params)
params_for_saving['n_params'] = mc.model.count_params()
save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

# Train model
mc.train()

# Evaluate (predicting on test set and plotting)
if eval and not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results
if eval and verbose:
    print('Evaluation scores:')
    print(mc.evaluation_scores)
