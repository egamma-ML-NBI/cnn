def get_params():
    """
    Returns a dictionary containing all parameters to be passed to the model
    container.

    Please see the README for documentation.
    """

    params = {

          # Training
          'epochs'                     : 100,
          'batch_size'                 : 256,
          'loss'                       : 'logcosh',
          'metrics'                    : ['mae'],
          'optimizer'                  : 'Nadam',
          'lr_finder'                  : {'use':False,
                                          'scan_range':[1e-4, 1e-2],
                                          'epochs':1,
                                          'prompt_for_input':False},
          'lr_schedule'                : {'name':None,
                                          'range':[1e-3,5e-3],
                                          'step_size_factor':5},
          'auto_lr'                    : False,
          'use_earlystopping'          : False,
          'restore_best_weights'       : True,
          'pretrained_model'           : {'use':False,
                                          'weights_path':'path/to/weights',
                                          'params_path':None,
                                          'layers_to_load':['cnn','top'],
                                          'freeze_loaded_layers':[True,False]},

          # Data
          'n_points'                   : {'train':None, 'val':None, 'test':None},
          'use_datagenerator'          : False,
          'use_sample_weights'         : False,
          'img_names'		           : None,
          'scalar_names'               : None,
          'eval_scalar_names'          : None,
          'use_times'                  : False,
          'use_tracks'                 : False,
          'upsampling'                 : {'use':False,
                                          'wanted_size':(56,55),
                                          'interpolation':'nearest'},

          # Submodels
          'top'                        : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[256,256,1],
                                          'final_activation':'linear'},
          'cnn'                        : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'cnn_type':'simple',
                                          'conv_dim':2,
                                          'block_depths':[1,4,4,4,4],
                                          'n_init_filters':32,
                                          'init_kernel_size':5,
                                          'rest_kernel_size':3,
                                          'globalavgpool':False,
                                          'downsampling':'maxpool',
                                          'min_size_for_downsampling':6},
          'scalar_net'                 : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[256],
                                          'connect_to':['FiLM_gen']},
          'FiLM_gen'                   : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'use':False,
                                          'units':[256,256]},
          'time_net'                   : {'initialization':'orthogonal',
                                          'activation':None,
                                          'normalization':None,
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[],
                                          'use_res':False,
                                          'final_activation':'pgauss_f',
                                          'final_activation_init':[0.5],
                                          'merge_method':'concatenate'},
          'track_net'                  : {'initialization':'orthogonal',
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'layer_reg':{},
                                          'dropout':None,
                                          'phi_units':[256,256],
                                          'rho_units':[256,256],
                                          'connect_to':['top','FiLM_gen']}
          }

    return params
