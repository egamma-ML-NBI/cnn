import os
import sys
import copy
import numpy as np
import itertools
from datetime import datetime
import h5py
import pickle
import argparse
import pprint


def load_data(path, n_points={'train':None, 'val':None, 'test':None},
              cnn_output=None, img_names=None, scalar_names=None,
              eval_scalar_names=None, load_sample_weights=False,
              load_time_imgs=False, load_tracks=False, verbose=True):
    """
    Args:
    -----
        path : *str*
            Path to the data to be loaded.

        n_points : *dict*
            This dictionary defines the names of the subsets of the data loaded
            (by way of the keys of `n_points`), as well as how many datapoints
            should be loaded for each set (by way of the values of `n_points`).
            This information is passed to `load_data` in `utils.py` through
            `main.py`.

            Its *keys* should be either `'train'` and `'val'`, or `'train'`,
            `'val'` and `'test'`. This means that while both training and
            validation data are required, test data can be left out. If test
            data is left out, no evaluation will be carried out at the end of
            training. It is important that the naming conventions of `'train'`,
            `'val'` and `'test'` are kept, as this is the only way the
            subsequent handling of the data knows which sets are meant to be
            used for what.

            The *values* of `n_points` dictate how many points of each set will
            be loaded. If `None`, all datapoints in the set corresponding to
            the key will be loaded. If instead a number n (*int* or *float*,
            where the latter will be converted into an *int*), only the first n
            datapoints of that set will be loaded. If a *list* of *int*s, this
            will be interpreted as indices, and only datapoints with these
            indices will be loaded (primarily used in conjunction with data
            generators).

        cnn_output : *str or None*
            The path of a file containing the outputs of a previously trained
            CNN. If not None, CNN outputs will be loaded instead of any images.

        img_names : *list of strs or None*
            List of image names to be loaded. Each image name will have its own
            entry in the returned data dict (acccessed by its name).
            If None (default), no images will be loaded and no CNN will be
            constructed.

        scalar_names : *list of strs or None*
            List of scalar variable names (meant to be used in training) to be
            loaded into the 'scalars' entry of the returned data dict.
            If None, no scalar variables will be loaded.

        eval_scalar_names : *list of strs or None*
            List of scalar variable names (meant to be used in evaluation) to be
            loaded into the 'eval_scalars' entry of the returned data dict.
            If 'all', all scalars will be loaded.
            If None, no evaluation scalar variables will be loaded.

        load_sample_weights : *bool*
            Whether to load sample weights to be used in training. Useful if
            classes are unbalanced or if certain marginal signal/background
            distributions do not follow each other nicely.

        load_time_imgs : *bool*
            Whether to load time images.

        load_tracks : *bool*
            Whether to load tracks.

        verbose : *bool*
            Verbose output.

    Returns:
    --------
        data : *dict*
            Dictionary of data. This dictionary (in its first level) will
            contain the same keys as those in n_points. Each of these keys point
            to another dict containing the different datasets (such as images
            and scalars).
            For example, if
            n_points = {'train':None, 'val':2000, 'test':1e3} and if
            scalar_names is not None, then we would access our training set
            scalars (which, like all the other datasets, should be a numpy
            array) like so: data['train']['scalars']. If we instead want
            to access the test set images, and img_names is ['img'], we do
            data['test']['img'].
            The target (or label in the case of classification) datasets are
            named 'Y'.
    """

    if verbose:
        print('Loading data.')

    # Prepare loading only the wanted data points
    # Make copy of n_points to use for loading (changes to n_points will persist
    # outside of this function)
    load_n_points = copy.deepcopy(n_points)

    for set_name in load_n_points:
        # If a list (of specific indices) is not given
        if not hasattr(load_n_points[set_name], '__iter__'):
            # If all data points should be loaded
            if load_n_points[set_name] is None:
                load_n_points[set_name] = slice(None)
            # If only the first int(load_n_points[set_name]) data points should be loaded
            else:
                load_n_points[set_name] = int(load_n_points[set_name])
                n_samples = load_n_points[set_name]
                load_n_points[set_name] = slice(None,load_n_points[set_name])
                if verbose:
                    print(f'Loading only the {n_samples} first data points of the {set_name} set.')
        else:
            load_n_points[set_name] = list(np.sort(load_n_points[set_name]))

    # Load the data
    with h5py.File(path, 'r') as hf:

        # Function for loading and organizing images
        def load_img(img_name, set_name, lrs, samples):
            return np.moveaxis(np.concatenate(tuple(hf[f'{img_name}_Lr{lr}_{set_name}'][samples] for lr in lrs),
                                              axis=1), source=1, destination=3)

        data = {set_name:{} for set_name in load_n_points}

        # Add targets to data
        for set_name in load_n_points:
            data[set_name]['Y'] = hf[f'y_{set_name}'][load_n_points[set_name]]

        # Add all scalars
        for set_name in load_n_points:
            data[set_name]['eval_scalars'] = hf[f'all_scalar_{set_name}'][load_n_points[set_name]]

        # Only load images if cnn_output is not already given
        if cnn_output is None and img_names is not None:
            # Add cell images to data
            # Different parts of the detector have a different number of layers
            layers_to_include = {}
            for img_name in img_names:
                if img_name == 'em_barrel':
                    lrs = [0,1,2,3]
                elif img_name == 'em_endcap':
                    lrs = [0,1,2,3]
                elif img_name == 'lAr_endcap':
                    lrs = [0,1,2,3]
                elif img_name == 'had_barrel':
                    lrs = [1,2]
                elif img_name == 'tile_ext_barrel':
                    lrs = [0,1,2,3]
                elif img_name == 'tile_gap':
                    lrs = [0,1,2,3]
                layers_to_include[img_name] = lrs

            for set_name in load_n_points:
                for img_name in img_names:
                    data[set_name][img_name] = load_img(img_name, set_name, layers_to_include[img_name], load_n_points[set_name])

            # Add time images to data
            # Note the division of 50 - this is actually preprocessing, but is easier to do here
            if load_time_imgs:
                for set_name in load_n_points:
                    for img_name in img_names:
                        data[set_name][f'time_{img_name}'] = load_img('time', set_name, layers_to_include[img_name], load_n_points[set_name]) / 50

        # Load the cnn_output, if given
        elif cnn_output is not None:
            with h5py.File(cnn_output,'r') as hf:
                for set_name in load_n_points:
                    data[set_name]['cnn_output'] = hf[f'cnn_ouputs_{set_name}'][load_n_points[set_name]]

        # Add scalars to data
        if scalar_names is not None:
            for set_name in load_n_points:
                data[set_name]['scalars'] = data[set_name]['eval_scalars'][scalar_names]

        # Reduce to only the variables that are actually needed (e.g. for plotting and evaluation)
        if eval_scalar_names is not None and eval_scalar_names!='all':
            for set_name in load_n_points:
                data[set_name]['eval_scalars'] = data[set_name]['eval_scalars'][eval_scalar_names]

        # Add sample weights (from reweighting)
        if load_sample_weights:
            for set_name in load_n_points:
                data[set_name]['sample_weights'] = data[set_name]['eval_scalars']['total_weight']

        # Add tracks
        if load_tracks:
            for set_name in load_n_points:
                data[set_name]['tracks'] = hf[f'track_{set_name}'][load_n_points[set_name]]

    # Convert scalar variables to normal numpy arrays (without names)
    # This can be done more elegantly!
    if scalar_names is not None:
        import pandas as pd
        for set_name in load_n_points:
            data[set_name]['scalars'] = pd.DataFrame(data[set_name]['scalars']).values

    if verbose:
        print('Data loaded.')

    return data


def create_directories(base_dir, epochs, log_prefix=''):

    current_time = datetime.utcnow().isoformat()

    if log_prefix != '':
        log_prefix = log_prefix.replace(',','_')
        log_dir = base_dir + f'logs/{log_prefix}/{current_time}_{epochs}_epochs/'
    else:
        log_dir = base_dir + f'logs/{current_time}_{epochs}_epochs/'

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    fig_dir = log_dir + 'figures/'
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)

    saved_models_dir = log_dir + 'saved_models/'
    if not os.path.exists(saved_models_dir):
        os.makedirs(saved_models_dir)

    lr_finder_dir = log_dir + 'lr_finder/'
    if not os.path.exists(lr_finder_dir):
        os.makedirs(lr_finder_dir)

    dirs = {'log'           : log_dir,
            'fig'           : fig_dir,
            'saved_models'  : saved_models_dir,
            'lr_finder'     : lr_finder_dir}

    return dirs


def merge_dicts(base_dict, subset_dict):
    """
    For every key in the dictionary subset_dict that is also a key in the
    base_dict dictionary, overwrite the value of that key in base_dict with the
    one from subset_dict, and return the resulting (partially overwritten)
    version of base_dict.
    """

    difference_dict = {key:base_dict[key] for key in base_dict if key not in subset_dict}
    return dict(subset_dict,**difference_dict)


def save_dict(dict, path, overwrite=False, save_pkl=False):
    """
    Args :
        dict :
            *dict*

            Dictionary to be saved.
        path :
            *str*

            Path to which the dictionary should be saved. The path must have a
            file ending (i.e. '.*') to work properly with the 'save_pkl' option.
            If the path already exists, the given dict will be appended
            to what is already in the file.
        overwrite :
            *bool*

            Whether to overwrite an already existing text file in the same path.
            Picled files are always overwritten, if they exist.
        save_pkl :
            *bool*

            Saves dict as a pickled file, in addition to the text file.
    """

    if os.path.exists(path):
        mod = 'a'
        if overwrite:
            mod = 'w+'
    else:
        mod = 'w+'

    with open(path, mod) as f:
        pprint.sorted = lambda x, key=None: x # disables sorting
        f.write(pprint.pformat(dict))

    if save_pkl:
        path_for_pkl = path.rsplit('.', 1)[0] + '.pkl'
        with open(path_for_pkl, 'wb+') as f:
            pickle.dump(dict,f)


def drop_key(dict, key_to_be_dropped):
    """Returns the input dictionary without the key_to_be_dropped, out of place."""

    if type(key_to_be_dropped)==str:
        if key_to_be_dropped in dict:
            return {key:dict[key] for key in dict if key!=key_to_be_dropped}
        else:
            return dict
    else: # List of keys to be dropped
        for key_in_list in key_to_be_dropped:
            dict = drop_key(dict, key_in_list)
        return dict


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def point_asdict(search_space_as_dict, point_as_list):

    return {key:val for key,val in zip(search_space_as_dict,point_as_list)}


def point_aslist(search_space_as_dict, point_as_dict):

    return [point_as_dict[key] for key in search_space_as_dict]


def str2bool(v):
    if v.lower() in ('true', 't', 'yes', 'y', '1'):
        return True
    elif v.lower() in ('false', 'f', 'no', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def get_str_combs(base, extension):
    fix = [base, extension]
    return [''.join(name) for name in itertools.chain.from_iterable([itertools.product(fix[i], ['','_'], fix[i+1]) for i in [0,-1]])]


def load_model(data, params, weights_path=None, verbose=False):
    """
    Create a model with parameters specified by params (a dictionary, as the one
    saved when running an experiment), and load weights saved in weights_path
    into it.
    """

    # Fixes relative imports
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/..')

    # Own imports
    from model_container import ModelContainer

    # Instantiate model container (with self.model in it)
    mc = ModelContainer(data=data,
                        params=params,
                        dirs=None,
                        save_figs=False,
                        verbose=verbose)

    # Load weights
    if weights_path is not None:
        mc.model.load_weights(weights_path)

    return mc
