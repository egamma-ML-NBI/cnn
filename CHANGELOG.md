# New in version ...

## 0.3.1

* Allowed for no metrics and no test set when doing hyperparameter search
* Changed the placement of DropBlock to be right after the activation function, instead of right after the convolution
* Provided a conda environment to be used in the tutorial, instead of the `requirements.txt` file, due to graphviz not working with pip (see https://github.com/ContinuumIO/anaconda-issues/issues/1666)

## 0.3

* Made repo backend agnostic
* Streamlined way of setting parameters for each submodel
* More featues (e.g., layer-, instance-, and group normalization, DropBlock, FiLM layers, and more)
* Updated docs
* Folder `aux` now only on the dev branch

## 0.2

* Addded inclusion of track data, along with a model (`track_net`) for processing them
* Added a model for processing scalars
* Standardized `time_net`, and added the option of a parametric time gate using a custom layer
* Generalized image loading, upsampling to common size and concatenation, all after name given in `param_conf.py`
* Added option to load data on the fly with a DataGenerator, for datasets that do not fit in memory
* Added `init_kernel_size` parameter
* Separated data and submodels in `param_conf.py`
* Added `use_loaded_model.py`, which contains a bunch of nice tools for looking into your models.

## 0.1

* Initial release
