import functools
import numpy as np
import copy
import keras as ks
import keras.backend as K

from keras_contrib.optimizers import Padam, Yogi
from keras_contrib.applications.resnet import ResNet
from keras_contrib.layers import GroupNormalization
from keras_drop_block import DropBlock2D
# Uncomment the following (and in get_optimizer) if you have backend set to Tensforflow and want to use on the listed optimizers
# from tensorflow.contrib.opt import AdamWOptimizer, PowerSignOptimizer, AddSignOptimizer, ShampooOptimizer

from utils import drop_key, merge_dicts, get_str_combs


def get_loss_function(loss, is_tensor=True):
    '''
    Returns a loss function.

    Works both for Keras tensors as well as numpy ndarrays, see is_tensor
    argument.

    Args:
    -----
        loss : *str*
            Name of loss function. Supported loss function names are:
                Keras in-built functions
                Custom functions:
                    'mse_sqrtnorm', 'msesqrt_sqrtnorm', 'mae_sqrtnorm',
                    'maesqrt_sqrtnorm', 'logcosh_sqrtnorm',
                    'logcoshsqrt_sqrtnorm', 'Z_abs', 'Z_sq'
                Keras in-built metrics (do not use as loss function)

        is_tensor : *bool, optional (default=True)*
            If True, get_loss_function returns a function that works with
            Keras tensors (of type keras.backeend.variable).
            If false, the returned function works with numpy ndarrays (of type
            numpy.ndarray).

            Technical note regarding speed: This is done through the decorator
            convert_if_not_tensor, which first converts inputs to the loss
            function to Keras tensors, call the function as if is_tensor was
            True, then converts the result back to a numpy.ndarray. This
            conversion in the case of the inputs being ndarrays adds some
            overhead, and is only intended to be used in an evaluation setting,
            as opposed to a training setting.

    Returns:
    --------
        *function*
    '''

    def convert_if_not_tensor(is_tensor):
        '''Decorator function for handling different datatypes.'''

        def actual_decorator(loss_function):
            @functools.wraps(loss_function)
            def wrapper(y_true, y_pred):
                if is_tensor:
                    return loss_function(y_true, y_pred)
                else:
                    y_true_as_tensor = K.variable(y_true)
                    y_pred_as_tensor = K.variable(y_pred)
                    return K.eval(loss_function(y_true_as_tensor, y_pred_as_tensor))
            return wrapper

        return actual_decorator

    try:
        return convert_if_not_tensor(is_tensor)(ks.losses.get(loss))
    except:
        pass

    try:
        return convert_if_not_tensor(is_tensor)(ks.metrics.get(loss))
    except:
        pass

    if loss.lower() in ['msesqrtnorm', 'mse_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def mse_sqrtnorm(y_true, y_pred):
            return K.mean(K.square(y_pred - y_true) / K.sqrt(y_true), axis=-1)
        return mse_sqrtnorm

    elif loss.lower() in ['msesqrtsqrtnorm', 'msesqrt_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def msesqrt_sqrtnorm(y_true, y_pred):
            return K.mean(K.square(K.sqrt(y_pred) - K.sqrt(y_true)) / K.sqrt(y_true), axis=-1)
        return msesqrt_sqrtnorm

    elif loss.lower() in ['maesqrtnorm', 'mae_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def mae_sqrtnorm(y_true, y_pred):
            return K.mean(K.abs(y_pred - y_true) / K.sqrt(y_true), axis=-1)
        return mae_sqrtnorm

    elif loss.lower() in ['maesqrtsqrtnorm', 'maesqrt_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def maesqrt_sqrtnorm(y_true, y_pred):
            return K.mean(K.abs(K.sqrt(y_pred) - K.sqrt(y_true)) / K.sqrt(y_true), axis=-1)
        return maesqrt_sqrtnorm

    elif loss.lower() in ['logcoshsqrtnorm', 'logcosh_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def logcosh_sqrtnorm(y_true, y_pred):
            def _logcosh(x):
                return x + K.softplus(-2. * x) - K.log(2.)
            return K.mean(_logcosh(y_pred - y_true) / K.sqrt(y_true), axis=-1)
        return logcosh_sqrtnorm

    elif loss.lower() in ['logcoshsqrtsqrtnorm', 'logcoshsqrt_sqrtnorm']:
        @convert_if_not_tensor(is_tensor)
        def logcoshsqrt_sqrtnorm(y_true, y_pred):
            def _logcosh(x):
                return x + K.softplus(-2. * x) - K.log(2.)
            return K.mean(_logcosh(K.sqrt(y_pred) - K.sqrt(y_true)) / K.sqrt(y_true), axis=-1)
        return logcoshsqrt_sqrtnorm

    elif loss.lower() in ['zabs', 'z_abs']:
        @convert_if_not_tensor(is_tensor)
        def Z_abs(y_true, y_pred):
            return K.mean(K.abs(y_pred - y_true / y_true), axis=-1)
        return Z_abs

    elif loss.lower() in ['zsq', 'z_sq']:
        @convert_if_not_tensor(is_tensor)
        def Z_sq(y_true, y_pred):
            return K.mean(K.pow(y_pred - y_true / y_true, 2), axis=-1)
        return Z_sq


def get_optimizer(optimizer):
    '''
    Returns :
        *Keras Optimizer instance*
    '''

    try:
        return ks.optimizers.get(optimizer)
    except:
        pass

    if isinstance(optimizer, dict):
        optimizer, kwargs = optimizer['class_name'], optimizer['config']
    else:
        kwargs = {}

    if optimizer.lower() in ['padam']: # Defaults: lr=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=0., amsgrad=False, partial=1. / 8.
        return Padam(**kwargs)

    elif optimizer.lower() in ['yogi']: # Defaults: lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-3, decay=0.
        return Yogi(**kwargs)

    # elif optimizer.lower() in ['adamw', 'adam_w']: # Defaults: As Adam
    #     tfoptimizer = AdamWOptimizer(**kwargs)
    #     return ks.optimizers.TFOptimizer(tfoptimizer)
    #
    # elif optimizer.lower() in ['powersign', 'power_sign']: # Defaults: learning_rate=0.1, base=math.e, beta=0.9, sign_decay_fn=None
    #     tfoptimizer = PowerSignOptimizer(**kwargs)
    #     return ks.optimizers.TFOptimizer(tfoptimizer)
    #
    # elif optimizer.lower() in ['addsign', 'add_sign']: # Defaults: learning_rate=0.1, alpha=1.0, beta=0.9
    #     tfoptimizer = AddSignOptimizer(**kwargs)
    #     return ks.optimizers.TFOptimizer(tfoptimizer)
    #
    # elif optimizer.lower() in ['shampoo']: # Defaults: global_step=0, max_matrix_size=768, gbar_decay=0.0, gbar_weight=1.0, mat_gbar_decay=1.0,
    #                                        # mat_gbar_weight=1.0, learning_rate=1.0, svd_interval=1 (20), precond_update_interval=1 (10),
    #                                        # epsilon=0.0001, alpha=0.5, use_iterative_root=False
    #     tfoptimizer = ShampooOptimizer(**kwargs)
    #     return ks.optimizers.TFOptimizer(tfoptimizer)

    else:
        raise NameError(f'Optimizer name {optimizer} not recognized.')


def get_auto_lr(optimizer_name, batch_size, range_factor=3):

    # Set scaling of learning rate based on optimizer
    if optimizer_name.lower() in ['nadam']:
        optimum_lr = 2.5*1e-5 * np.sqrt(batch_size)

    elif optimizer_name.lower() in ['padam']:
        optimum_lr = 1e-3 * np.sqrt(batch_size)

    elif optimizer_name.lower() in ['yogi']:
        optimum_lr = 3*1e-5 * np.sqrt(batch_size)

    else:
        raise NotImplementedError(f'Optimizer {optimizer_name} not implemented in get_auto_lr.')

    # Adjust the cyclical learning rate schedule to vary around the set value
    range_factor = 3

    return optimum_lr, [optimum_lr * range_factor**(-0.5),
                        optimum_lr * range_factor**(0.5)]


def crop(dimension, start=None, end=None, squeeze_axis=None):
    """
    Crops (or slices) a Tensor on a given dimension from start to end
    Example : to crop tensor x[:, :, 5:10], call crop(2, 5, 10) as you want to
    crop on the second dimension
    """
    def func(x):
        if dimension == 0:
            x = x[start:end]
        if dimension == 1:
            x = x[:, start:end]
        if dimension == 2:
            x = x[:, :, start:end]
        if dimension == 3:
            x = x[:, :, :, start:end]
        if dimension == 4:
            x = x[:, :, :, :, start:end]

        if squeeze_axis is not None:
            x = K.squeeze(x,squeeze_axis)

        return x

    return ks.layers.Lambda(func)


def get_activation(activation='linear'):
    '''
    Adds an activation layer to a graph.

    Args:
    -----
        activation : *str or config dict, optional (default='linear')*
            The name of an activation function.
            One of 'relu', 'leakyrelu', 'prelu', 'elu', 'mrelu', 'swish',
            'gauss', 'gauss_f', 'pgauss', 'pgauss_f' (or any of their
            aliases), or anything that Keras will recognize as an
            activation function name.

    Returns:
    --------
        *Keras layer instance*
    '''
    if activation is None:
        activation = 'linear'

    if isinstance(activation, dict):
        activation, kwargs = activation['class_name'], activation['config']
    else:
        kwargs = {}

    if activation.lower() in ['relu']:
        act = ks.layers.ReLU(**kwargs)

    elif activation.lower() in ['leakyrelu', 'leaky_relu']:
        act = ks.layers.LeakyReLU(**kwargs)

    elif activation.lower() in ['prelu', 'p_relu']:
        act = ks.layers.PReLU(**kwargs)

    elif activation.lower() in ['elu']:
        act = ks.layers.ELU(**kwargs)

    elif activation.lower() in ['swish']:
        def swish(x):
            return K.sigmoid(x) * x
        act = ks.layers.Activation(swish, **kwargs)

    elif activation.lower() in ['mrelu', 'm_relu']:
        def mrelu(x):
            return K.minimum(K.maximum(1-x, 0), K.maximum(1+x, 0))
        act = ks.layers.Activation(mrelu, **kwargs)

    elif activation.lower() in ['gauss', 'gaussian']:
        def gauss(x):
            return K.exp(-x**2)
        act = ks.layers.Activation(gauss, **kwargs)

    elif activation.lower() in get_str_combs(['gauss','gaussian'], ['f','flipped']):
        def gauss_f(x):
            return 1 - K.exp(-x**2)
        act = ks.layers.Activation(gauss_f, **kwargs)

    elif activation.lower() in get_str_combs(['gauss','gaussian'], ['p','parametric']):
        act = ParametricGate(flipped=False, **kwargs)

    elif activation.lower() in get_str_combs(get_str_combs(['gauss','gaussian'], ['p','parametric']), ['f', 'flipped']):
        act = ParametricGate(flipped=True, **kwargs)

    else:
        act = ks.layers.Activation(activation, **kwargs)

    return act


def get_downsampling(tns, downsampling):
    '''
    Adds downsampling layer to a graph.

    Note that no kernel_initializer or regularization arguments are currently
    passed to the conv layer (when downsampling=='strided').

    Args:
    -----
        tns : *Keras tensor*
            Input tensor.

        downsampling : *str*
            The wanted way of downsampling.
            One of 'avgpool', 'maxpool' or 'strided'.

    Returns:
    --------
        *Keras tensor*
    '''

    input_shape = tns._keras_shape

    # Determine pool filter size
    if all(dim is None for dim in input_shape[1:3]):
        hw_factor = 1
        wh_factor = 1
    else:
        height = input_shape[1]
        width = input_shape[2]
        if height >= 1.5*width:
            hw_factor = 1.5
            wh_factor = 1
        elif width >= 1.5*height:
            hw_factor = 1
            wh_factor = 1.5
        else:
            hw_factor = 1
            wh_factor = 1

    if len(input_shape) == 4: # 2D convolution
        pool_size = (np.round(2*hw_factor).astype(np.integer),
                     np.round(2*wh_factor).astype(np.integer))

        if downsampling == 'avgpool':
            return ks.layers.AveragePooling2D(pool_size)(tns)
        elif downsampling == 'maxpool':
            return ks.layers.MaxPooling2D(pool_size)(tns)
        elif downsampling == 'strided':
            n_channels = input_shape[-1] # Don't change the number of channels
            return ks.layers.Conv2D(filters=n_channels, kernel_size=pool_size, strides=pool_size)(tns)

    elif len(input_shape) == 5: # 3D convolution
        pool_size = (int(2*hw_factor),int(2*wh_factor),1)

        if downsampling == 'avgpool':
            return ks.layers.AveragePooling3D(pool_size)(tns)
        elif downsampling == 'maxpool':
            return ks.layers.MaxPooling3D(pool_size)(tns)
        elif downsampling == 'strided':
            n_channels = input_shape[-1] # Don't change the number of channels
            return ks.layers.Conv3D(filters=n_channels, kernel_size=pool_size, strides=pool_size)(tns)

    else:
        print('WARNING: Input not suitable for neither 2D nor 3D downsampling. Continuing without downsampling.')
        return tns


def get_normalization(tns=None, normalization='batch', freeze=False):
    '''
    Adds a normalization layer to a graph.

    Args:
    -----
        tns : *Keras tensor or None*
            Input tensor. If not None, then the graph will be connected through
            it, and a tensor will be returned. If None, the normalization layer
            will be returned.

        normalization : *str or config dict, optional (default='batch')*

            The name of an normalization function.
            One of 'batch', 'layer', 'instance', or 'group' (or their aliases).
        freeze : *bool, optional (default=False)*
            Whether the beta and gamma parameters of normalization layer should
            be frozen or not.

    Returns:
    --------
        *Keras tensor or layer instance* (see tns argument)
    '''
    if isinstance(normalization, dict):
        normalization, config = normalization['class_name'], normalization['config']
    else:
        config = {}

    if freeze:
        freeze_kwargs = {'center':False, 'scale':False}
    else:
        freeze_kwargs = {}

    # Merge kwarg dicts
    kwargs = merge_dicts(config, freeze_kwargs)

    if normalization.lower() in get_str_combs(['batch'], ['norm','normalization','']):
        norm = ks.layers.BatchNormalization(**kwargs)

    elif normalization.lower() in get_str_combs(['layer'], ['norm','normalization','']):
        norm = GroupNormalization(groups=1, **kwargs)

    elif normalization.lower() in get_str_combs(['instance'], ['norm','normalization','']):
        if tns is None:
            raise Exception('Instance normalization needs a tns to be passed to '
                            'get_normalization.')
        n_features = tns._keras_shape[-1]
        norm = GroupNormalization(groups=n_features, **kwargs)

    elif normalization.lower() in get_str_combs(['group'], ['norm','normalization','']):
        norm = GroupNormalization(**kwargs)

    else:
        raise NameError(f'Normalization name {normalization} not recognized.')

    if tns is not None:
        return norm(tns)
    else:
        return norm


def deserialize_layer_reg(layer_reg):

    deserialized_layer_reg = copy.deepcopy(layer_reg)

    for reg_type in deserialized_layer_reg:
        if reg_type in ['kernel_regularizer', 'bias_regularizer', 'activity_regularizer']:
            deserialized_layer_reg[reg_type] = ks.regularizers.get(deserialized_layer_reg[reg_type])
        elif reg_type in ['kernel_constraint', 'bias_constraint']:
            deserialized_layer_reg[reg_type] = ks.constraints.get(deserialized_layer_reg[reg_type])

    return deserialized_layer_reg


def upsample_img(tns, normalize=False, size=(1,1), interpolation='nearest'):

    def apply_normalization(tns):
        return tns / np.prod(size)
    tns = ks.layers.UpSampling2D(size=size, interpolation=interpolation,
                                 data_format='channels_last')(tns)
    if normalize:
        tns = ks.layers.Lambda(apply_normalization)(tns)

    return tns


def get_norm_act(tns, activation=None, normalization=None, FiLM_tns=None):

    # Apply normalization
    if normalization is not None:
        tns = get_normalization(tns, normalization=normalization,
                                freeze=FiLM_tns is not None)

    # Condition using output of FiLM_gen
    if FiLM_tns is not None:
        tns = FiLM()([tns, FiLM_tns])

    return get_activation(activation)(tns)


def get_dense_norm_act(tns, units, initialization='orthogonal',
                       activation=None, normalization=None, layer_reg={},
                       dropout=None):

    for unit in units:
        tns = ks.layers.Dense(unit,
                              kernel_initializer=ks.initializers.get(initialization),
                              use_bias=False,
                              **deserialize_layer_reg(layer_reg))(tns)
        if dropout is not None:
            tns = ks.layers.Dropout(dropout)(tns)
        tns = get_norm_act(tns, activation, normalization)

    return tns





########## SUBMODELS ##########

def get_top(input_shape, units=[256,256,1], final_activation=None,
            initialization='orthogonal', activation=None, normalization=None,
            layer_reg={}, dropout=None):
    '''Creates a model for the dense top.'''

    input = ks.layers.Input(input_shape)
    tns = input

    # Flatten if needed
    if len(input_shape) > 1:
        tns = ks.layers.Flatten()(tns)

    # Hidden layers
    tns = get_dense_norm_act(tns, units=units[:-1],
                             initialization=initialization,
                             activation=activation,
                             normalization=normalization,
                             layer_reg=layer_reg,
                             dropout=dropout)

    # Output neuron(s)
    tns = ks.layers.Dense(units[-1], activation=final_activation)(tns)

    return ks.models.Model(input, tns, name='top')


def get_cnn(input_shape, FiLM_input_shapes=None,
            cnn_type='simple', conv_dim=2, n_init_filters=32, block_depths=[1,2,2],
            init_kernel_size=3, rest_kernel_size=3,
            downsampling=None, min_size_for_downsampling=6,
            globalavgpool=False, FiLM_tns=None,
            initialization='orthogonal', activation=None, normalization=None,
            layer_reg={}, dropout=None):
    '''
    Returns a CNN model.

    Args:
    -----
        input_shape : *tuple*

        FiLM_input_shapes : *list of tuples or None*
             The shapes of the list of outputs of the FiLM generator, consisting
             of the biases and scalings to be applied to each feature map in the
             first convolutional layer of each block in the CNN.
             If None, FiLM conditioning will not be used.

        **kwargs :
            See the docs in the README for a description of the rest of the
            arguments.

    Returns:
    --------
        *Instantiated Keras model*
    '''

    # Input
    input = ks.layers.Input(input_shape)
    tns = input

    if FiLM_input_shapes is not None:
        inputs_FiLM = {}
        for i,FiLM_input_shape in enumerate(FiLM_input_shapes):
            inputs_FiLM[f'FiLM_{i}'] = ks.layers.Input(FiLM_input_shape, name=f'FiLM_gen_output_{i}')
        FiLM_tns_list = [inputs_FiLM[key] for key in inputs_FiLM]

    # Skip everything else if cnn_type is 'res18'
    # You can return other readily available models from keras_contrib here
    if cnn_type == 'res18':
        return ResNet(input_shape=input_shape,
                      block='basic',
                      repetitions=[2,2,2,2],
                      include_top=False,
                      initial_strides=(1,1),
                      initial_kernel_size=init_kernel_size,
                      final_pooling='avg')

    # If chosen, reshape to be able to use 3D convolutions (with number of channels now being 1)
    if conv_dim == 3:
        input_shape_tmp = tns._keras_shape[1:] # Batch size (None) should not be included
        tns = ks.layers.Reshape(input_shape_tmp + (1,))(tns)
        if FiLM_input_shapes is not None:
            raise Exception('3D convolutions and FiLM layers are currently not compatible.')

    # Define convolutional module
    def conv_module(tns, n_filters, kernel_size=3, cnn_type='simple',
                    downsample=False, FiLM_tns=None):

        def conv_drop_norm_act(tns, kernel_size=3, use_bias=True,
                               norm_first=False, FiLM_tns=None):
            """Convenience function to use for both simple and res type CNNs."""

            def _get_norm_act_dropblock(tns):

                tns = get_norm_act(tns, activation, normalization, FiLM_tns)

                # Dropblock
                if dropout is not None:
                    if conv_dim == 3:
                        raise Exception('3D convolutions and DrobBlock layers are '
                                        'currently not compatible.')
                    if not isinstance(dropout, dict):
                        raise TypeError('Please pass a dict of keyword arguments to '
                                        'the DropBlock layer in the CNN.')
                    else:
                        tns = DropBlock2D(**dropout)(tns)

                return tns

            # Normalization first
            if norm_first:
                tns = _get_norm_act_dropblock(tns)

            # Convolution
            if conv_dim == 2:
                tns = ks.layers.Conv2D(n_filters, kernel_size, use_bias=use_bias, padding='same',
                                       kernel_initializer=ks.initializers.get(initialization),
                                       **deserialize_layer_reg(layer_reg))(tns)
            elif conv_dim == 3:
                if not type(kernel_size)==tuple:
                    kernel_size = (kernel_size,)*2 + (2,)
                tns = ks.layers.Conv3D(n_filters, kernel_size, use_bias=use_bias, padding='same',
                                       kernel_initializer=ks.initializers.get(initialization),
                                       **deserialize_layer_reg(layer_reg))(tns)

            # Normalization last
            if not norm_first:
                tns = _get_norm_act_dropblock(tns)

            return tns

        # Downsample
        if downsample and downsampling is not None:
            # Check if H,W are dynamic - if yes, do downsampling and hope for the best
            if all(dim is None for dim in tns._keras_shape[1:3]):
                tns = get_downsampling(tns, downsampling)
            else:
                # Check that both height and width are at least min_size_for_downsampling
                if all(dim >= min_size_for_downsampling for dim in tns._keras_shape[1:3]):
                    tns = get_downsampling(tns, downsampling)

        if cnn_type == 'simple':
            return conv_drop_norm_act(tns, kernel_size=kernel_size,
                                      use_bias=True, norm_first=False,
                                      FiLM_tns=FiLM_tns)

        elif cnn_type == 'res':
            # Project shortcut so it has compatible number of output feature maps
            if downsample:
                if conv_dim == 2:
                    shortcut = ks.layers.Conv2D(n_filters, (1,1), padding='same',
                                           kernel_initializer=ks.initializers.get(initialization),
                                           **deserialize_layer_reg(layer_reg))(tns)
                elif conv_dim == 3:
                    shortcut = ks.layers.Conv3D(n_filters, (1,1,1), padding='same',
                                           kernel_initializer=ks.initializers.get(initialization),
                                           **deserialize_layer_reg(layer_reg))(tns)
            else:
                shortcut = tns

            # First conv
            tns = conv_drop_norm_act(tns, kernel_size=kernel_size,
                                     use_bias=normalization is None,
                                     norm_first=True, FiLM_tns=None)

            # Second conv
            tns = conv_drop_norm_act(tns, kernel_size=kernel_size,
                                     use_bias=True, norm_first=True,
                                     FiLM_tns=FiLM_tns)

            return ks.layers.Add()([tns, shortcut])

    # First block (simple convolution, no matter type chosen)
    for j in range(block_depths[0]):
        tns = conv_module(tns, n_init_filters, kernel_size=init_kernel_size,
                          cnn_type='simple', downsample=False,
                          FiLM_tns=(FiLM_tns_list[0] if FiLM_input_shapes is not None and j==0 else None))

    # Remaining blocks
    n_filters = n_init_filters

    for i,depth in enumerate(block_depths[1:]):
        n_filters *= 2
        for j in range(depth):
            tns = conv_module(tns, n_filters, kernel_size=rest_kernel_size,
                              cnn_type=cnn_type, downsample=(j==0),
                              FiLM_tns=(FiLM_tns_list[i+1] if FiLM_input_shapes is not None and j==0 else None))

    # Apply global average pooling to reduce the number of features, or simply flatten
    if globalavgpool:
        if conv_dim == 2:
            tns = ks.layers.GlobalAveragePooling2D()(tns)
        elif conv_dim == 3:
            tns = ks.layers.GlobalAveragePooling3D()(tns)
    else:
        # Dynamic flatten
        if all(dim is None for dim in tns._keras_shape[1:3]):
            tns = ks.layers.Lambda(lambda x: K.batch_flatten(x))(tns)
        # Normal flatten
        else:
            tns = ks.layers.Flatten()(tns)

    return ks.models.Model(inputs=(input if FiLM_input_shapes is None else [input] + FiLM_tns_list),
                           outputs=tns, name='cnn')


def get_FiLM_generator(input_shape, n_blocks, n_init_filters, units=[256,256],
                       initialization='orthogonal', activation=None,
                       normalization=None, layer_reg={}, dropout=None):

    # Inputs
    input = ks.layers.Input(input_shape, name='FiLM_scalars')

    # Calculate how many biases and scaling factors are needed
    n_outputs = 2 * n_init_filters * (2**n_blocks - 1)

    # Hidden layers
    tns = get_dense_norm_act(input, units=units,
                             initialization=initialization,
                             activation=activation,
                             normalization=normalization,
                             layer_reg=layer_reg,
                             dropout=dropout)

    # Output neurons
    tns = ks.layers.Dense(n_outputs, **deserialize_layer_reg(layer_reg))(tns)

    # Reshape such that it is transparent which outputs should modify which conv outputs
    tns_list = []
    start = 0
    stop = 2 * n_init_filters
    for i in range(n_blocks):
        tns_list.append(crop(1, start, stop)(tns))
        start, stop = stop, stop + 2 * n_init_filters * 2**(i+1)

    return ks.models.Model(input, tns_list, name='FiLM_generator')


def get_time_net(units=[64,64], use_res=False, final_activation='pgauss_f',
                 final_activation_init=[0.5], initialization='orthogonal',
                 activation=None, normalization=None, layer_reg={}, dropout=None):

    # Do pixel-wise transformation
    input = ks.layers.Input((1,))
    tns = input

    # Hidden layers
    tns = get_dense_norm_act(tns, units=units,
                             initialization=initialization,
                             activation=activation,
                             normalization=normalization,
                             layer_reg=layer_reg,
                             dropout=dropout)

    # Output neuron
    if units:
        tns = ks.layers.Dense(1)(tns)
        if use_res:
            tns = ks.layers.Add()([input, tns])

    # Apply gate activation
    tns = get_activation(final_activation)(tns)

    # Make model
    model = ks.models.Model(input, tns, name='time_net')

    # Set initial weights, if using a parametric activation function
    if final_activation.lower() in ['pgauss', 'pgauss_f', 'pgauss_flipped']: # TODO: Doesn't cover all aliases
        init_weights = [np.array([init],dtype='float32') for init in final_activation_init]
        model.layers[-1].set_weights(init_weights)

    return model


def get_scalar_net(input_shape, units=[256,256],
                   initialization='orthogonal', activation=None,
                   normalization=None, layer_reg={}, dropout=None):
    '''Creates a model for processing (representing) the scalar variables.'''

    input = ks.layers.Input(input_shape)
    tns = input

    # Flatten if needed
    if len(input_shape) > 1:
        tns = ks.layers.Flatten()(tns)

    tns = get_dense_norm_act(tns, units=units,
                             initialization=initialization,
                             activation=activation,
                             normalization=normalization,
                             layer_reg=layer_reg,
                             dropout=dropout)

    return ks.models.Model(input, tns, name='scalar_net')


def get_track_net(input_shape, phi_units=[256,256], rho_units=[256,256],
                  initialization='orthogonal', activation=None,
                  normalization=None, layer_reg={}, dropout=None):
    '''
    Creates a model for processing (representing) tracks, of which there can
    be a varying amount for each event.

    The input should have shape (n_tracks, n_features) (note that batch_size
    should not be included), where n_tracks is the maximum amount of tracks in
    the whole dataset, and where n_features is the number of features for each
    track. If a given datapoint has fewer than n_tracks tracks, the rest should
    be zero-padded. These padded tracks are subsequently masked out.
    '''

    # Inputs
    input = ks.layers.Input(input_shape)
    tns = input

    apply_mask = True

    # Implement mask manually. For why this is necessary, see https://github.com/keras-team/keras/issues/10320
    if apply_mask:
        # Construct
        mask = ks.layers.Lambda(lambda x: K.cast(K.all(K.not_equal(x, 0), axis=2), 'float32'), name='mask')(tns)

    # Define the inner model (phi), passed to TimeDistributed
    input_inner = ks.layers.Input((input_shape[-1],))

    tns_inner = get_dense_norm_act(input_inner, units=phi_units,
                                  initialization=initialization,
                                  activation=activation,
                                  normalization=normalization,
                                  layer_reg=layer_reg,
                                  dropout=dropout)

    inner_model = ks.models.Model(input_inner, tns_inner, name='phi')

    # Pass each track through the inner_model
    tns = ks.layers.TimeDistributed(inner_model)(tns)

    if apply_mask:
        # Reshape mask so that it can be multiplied with the output of TimeDistributed
        mask = ks.layers.Lambda(lambda x: K.expand_dims(x), name='expand')(mask)

        # Apply the mask
        tns = ks.layers.multiply([mask, tns])

    # Sum the outputs of inner_model for each track (to invoke permutation invariance)
    tns = ks.layers.Lambda(lambda x: K.sum(x, axis=1), name='sum')(tns)

    # Pass the summed outputs through the final transformation (rho)
    tns = get_dense_norm_act(tns, units=rho_units,
                             initialization=initialization,
                             activation=activation,
                             normalization=normalization,
                             layer_reg=layer_reg,
                             dropout=dropout)

    return ks.models.Model(input, tns, name='track_net')



########## CUSTOM LAYERS ##########

class ParametricGate(ks.layers.Layer):

    def __init__(self, flipped=False, **kwargs):
        self.flipped = flipped
        super(ParametricGate, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create trainable weight variables for this layer.
        self.alpha = self.add_weight(name='alpha',
                                      shape=(1,),
                                      initializer='uniform',
                                      trainable=True)

        super(ParametricGate, self).build(input_shape)

    def call(self, x):
        def pgauss(x, sigma, flipped=False):
            if flipped:
                return 1 - K.exp(-(x/sigma)**2)
            else:
                return K.exp(-(x/sigma)**2)
        return pgauss(x, self.alpha, flipped=self.flipped)

    def compute_output_shape(self, input_shape):
        return input_shape # shouldn't change!


class FiLM(ks.layers.Layer):

    def __init__(self, **kwargs):
        super(FiLM, self).__init__(**kwargs)

    def build(self, input_shape):
        assert isinstance(input_shape, list)
        feature_map_shape, FiLM_tns_shape = input_shape
        self.height = feature_map_shape[1]
        self.width = feature_map_shape[2]
        self.n_feature_maps = feature_map_shape[-1]
        assert(int(2 * self.n_feature_maps)==FiLM_tns_shape[1])
        super(FiLM, self).build(input_shape)

    def call(self, x):
        assert isinstance(x, list)
        conv_output, FiLM_tns = x

        # Duplicate in order to apply to entire feature maps
        # Taken from https://github.com/GuessWhatGame/neural_toolbox/blob/master/film_layer.py
        FiLM_tns = K.expand_dims(FiLM_tns, axis=[1])
        FiLM_tns = K.expand_dims(FiLM_tns, axis=[1])
        FiLM_tns = K.tile(FiLM_tns, [1, self.height, self.width, 1])

        # Split into gammas and betas
        gammas = FiLM_tns[:, :, :, :self.n_feature_maps]
        betas = FiLM_tns[:, :, :, self.n_feature_maps:]

        # Apply affine transformation
        return (1 + gammas) * conv_output + betas

    def compute_output_shape(self, input_shape):
        assert isinstance(input_shape, list)
        return input_shape[0]
