"""
Contains all custom classes used by ModelContainer in model_container.py, except
for custom layers, which are in model_building_functions.py.
"""

import numpy as np
import h5py
import keras as ks
import keras.backend as K

from utils import load_data


class DataGenerator(ks.utils.Sequence):
    '''Generates data for Keras.

    From https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly'''

    def __init__(self, set_name, params, n_samples, predict=False, shuffle=True):
        'Initialization'
        self.set_name = set_name
        self.params = params
        self.n_samples = n_samples
        self.indices = np.arange(self.n_samples)
        self.predict = predict
        self.shuffle = shuffle
        self.on_epoch_end()


    def __len__(self):
        'Denotes the number of mini-batches per epoch'
        return int(np.ceil(self.n_samples / self.params['batch_size'])) # NOTE: Was originally np.floor()


    def __getitem__(self, index):
        '''Generate one batch of data.

        index is the mini-batch index (the last index is self.__len__())
        '''

        # Generate indices of the batch
        batch_indices = self.indices[index*self.params['batch_size']:(index+1)*self.params['batch_size']]

        return self._generate_data(batch_indices)


    def on_epoch_end(self):
        'Updates indices after each epoch'
        if self.shuffle == True:
            np.random.shuffle(self.indices)


    def _generate_data(self, batch_indices): # NOTE: Was originally __data_generation
        'Generates data containing batch_size samples'

        # Modify n_points to have the batch_indices
        n_points = {self.set_name:batch_indices}

        # Load the wanted the data
        data = load_data(self.params['data_path'],
                         n_points=n_points,
                         img_names=self.params['img_names'],
                         scalar_names=self.params['scalar_names'],
                         eval_scalar_names=self.params['eval_scalar_names'],
                         load_sample_weights=self.params['use_sample_weights'],
                         load_time_imgs=self.params['use_times'],
                         load_tracks=self.params['use_tracks'],
                         verbose=False)

        # Organize the data into lists of the form that the Keras model expects
        return self._organize_data(data, self.set_name)


    def _organize_data(self, data, set_name):
        '''Data should be added in the same order as inputs.'''

        x = []

        if self.params['img_names'] is not None:
            # Standard images
            for img_name in self.params['img_names']:
                x.append(data[set_name][img_name])

            # Time images
            if self.params['use_times']:
                for img_name in self.params['img_names']:
                    x.append(data[set_name][f'time_{img_name}'])

        # Scalars
        if self.params['scalar_names'] is not None:
            x.append(data[set_name]['scalars'])

        # Tracks
        if self.params['use_tracks']:
            x.append(data[set_name]['tracks'])

        # Targets
        y = data[set_name]['Y']

        if self.predict:
            return x
        else:
            if self.params['use_sample_weights']:
                sample_weights = data[set_name]['sample_weights']
                return x, y, sample_weights
            else:
                return x, y


# CALLBACKS

from keras.callbacks import Callback

import matplotlib.pyplot as plt
plt.switch_backend('agg') # For outputting plots when running on a server
plt.ioff() # Turn off interactive mode, so that figures are only saved, not displayed
plt.rcParams.update({'font.size': 18})

class LRFinder(Callback):

    '''
    A simple callback for finding the optimal learning rate range for your model + dataset.

    # Usage
        ```python
            lr_finder = LRFinder(min_lr=1e-5,
                                 max_lr=1e-2,
                                 steps_per_epoch=np.ceil(epoch_size/batch_size),
                                 epochs=3)
            model.fit(X_train, Y_train, callbacks=[lr_finder])

            lr_finder.plot_loss_vs_lr()
        ```

    # Arguments
        min_lr: The lower bound of the learning rate range for the experiment.
        max_lr: The upper bound of the learning rate range for the experiment.
        steps_per_epoch: Number of mini-batches in the dataset. Calculated as `np.ceil(epoch_size/batch_size)`.
        epochs: Number of epochs to run experiment. Usually between 2 and 4 epochs is sufficient.

    # References
        Blog post: jeremyjordan.me/nn-learning-rate
        Original paper: https://arxiv.org/abs/1506.01186
    '''

    def __init__(self, min_lr=1e-5, max_lr=1e-2, steps_per_epoch=None, epochs=None, fig_dir='./'):
        super().__init__()

        self.min_lr = min_lr
        self.max_lr = max_lr
        self.total_iterations = steps_per_epoch * epochs
        self.iteration = 0
        self.history = {}
        self.fig_dir = fig_dir

    def clr(self):
        '''Calculate the learning rate.'''
        x = self.iteration / self.total_iterations
        return self.min_lr + (self.max_lr-self.min_lr) * x

    def on_train_begin(self, logs=None):
        '''Initialize the learning rate to the minimum value at the start of training.'''
        logs = logs or {}
        if hasattr(self.model.optimizer, 'lr'):
            K.set_value(self.model.optimizer.lr, self.min_lr)
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.min_lr)

    def on_batch_end(self, epoch, logs=None):
        '''Record previous batch statistics and update the learning rate.'''
        logs = logs or {}
        self.iteration += 1

        if hasattr(self.model.optimizer, 'lr'):
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        else:
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.optimizer._lr))

        self.history.setdefault('iterations', []).append(self.iteration)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        if hasattr(self.model.optimizer, 'lr'):
            K.set_value(self.model.optimizer.lr, self.clr())
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.clr())

    def plot_lr(self):
        '''Helper function to quickly inspect the learning rate schedule.'''

        plt.plot(self.history['iterations'], self.history['lr'])
        plt.xlabel('Iteration')
        plt.ylabel('Learning rate')
        # plt.yscale('log')
        plt.tight_layout()
        plt.savefig(self.fig_dir + 'lr.pdf')
        plt.close()

    def plot_loss_vs_lr(self, chosen_limits=None):
        '''Helper function to quickly observe the learning rate experiment results.'''

        mask = np.array(self.history['loss']) < 1e3
        lrs = np.array(self.history['lr'])[mask]
        losses = np.array(self.history['loss'])[mask]

        fig,ax = plt.subplots()
        ax.plot(lrs, losses)
        ax.set_xscale('log')
        ax.set_xlabel('Learning rate')
        ax.set_ylabel('Loss')
        if chosen_limits is not None:
            ax.axvline(x=chosen_limits[0], color='k', ls=':', label='Chosen limits')
            ax.axvline(x=chosen_limits[1], color='k', ls=':')
        plt.tight_layout()
        plt.savefig(self.fig_dir + 'loss_vs_lr.pdf')
        plt.close()

        with h5py.File(self.fig_dir + 'lr_vs_loss.h5', 'w') as h:
            h.create_dataset('lr', data=lrs)
            h.create_dataset('loss', data=losses)


class SGDRScheduler(Callback):
    '''Cosine annealing learning rate scheduler with periodic restarts.

    # Usage
        ```python
            schedule = SGDRScheduler(min_lr=1e-5,
                                     max_lr=1e-2,
                                     steps_per_epoch=np.ceil(epoch_size/batch_size),
                                     lr_decay=0.9,
                                     cycle_length=5,
                                     mult_factor=1.5)
            model.fit(X_train, Y_train, epochs=100, callbacks=[schedule])
        ```

    # Arguments
        min_lr: The lower bound of the learning rate range for the experiment.
        max_lr: The upper bound of the learning rate range for the experiment.
        steps_per_epoch: Number of mini-batches in the dataset. Calculated as `np.ceil(epoch_size/batch_size)`.
        lr_decay: Reduce the max_lr after the completion of each cycle.
                  Ex. To reduce the max_lr by 20% after each cycle, set this value to 0.8.
        cycle_length: Initial number of epochs in a cycle.
        mult_factor: Scale epochs_to_restart after each full cycle completion.

    # References
        Blog post: jeremyjordan.me/nn-learning-rate
        Original paper: http://arxiv.org/abs/1608.03983
    '''
    def __init__(self,
                 min_lr,
                 max_lr,
                 steps_per_epoch,
                 lr_decay=1,
                 cycle_length=10,
                 mult_factor=2):

        self.min_lr = min_lr
        self.max_lr = max_lr
        self.lr_decay = lr_decay

        self.batch_since_restart = 0
        self.next_restart = cycle_length

        self.steps_per_epoch = steps_per_epoch

        self.cycle_length = cycle_length
        self.mult_factor = mult_factor

        self.history = {}

    def clr(self):
        '''Calculate the learning rate.'''
        fraction_to_restart = self.batch_since_restart / (self.steps_per_epoch * self.cycle_length)
        lr = self.min_lr + 0.5 * (self.max_lr - self.min_lr) * (1 + np.cos(fraction_to_restart * np.pi))
        return lr

    def on_train_begin(self, logs={}):
        '''Initialize the learning rate to the minimum value at the start of training.'''
        logs = logs or {}
        if hasattr(self.model.optimizer, 'lr'):
            K.set_value(self.model.optimizer.lr, self.max_lr)
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.max_lr)

    def on_batch_end(self, batch, logs={}):
        '''Record previous batch statistics and update the learning rate.'''
        logs = logs or {}

        if hasattr(self.model.optimizer, 'lr'):
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        else:
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.optimizer._lr))

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        self.batch_since_restart += 1

        if hasattr(self.model.optimizer, 'lr'):
            K.set_value(self.model.optimizer.lr, self.clr())
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.clr())

    def on_epoch_end(self, epoch, logs={}):
        '''Check for end of current cycle, apply restarts when necessary.'''
        if epoch + 1 == self.next_restart:
            self.batch_since_restart = 0
            self.cycle_length = np.ceil(self.cycle_length * self.mult_factor)
            self.next_restart += self.cycle_length
            self.max_lr *= self.lr_decay
            self.best_weights = self.model.get_weights()

    def on_train_end(self, logs={}):
        '''Set weights to the values from the end of the most recent cycle for best performance.'''
        self.model.set_weights(self.best_weights)



from keras.callbacks import *

class CyclicLR(Callback):
    """This callback implements a cyclical learning rate policy (CLR).
    The method cycles the learning rate between two boundaries with
    some constant frequency, as detailed in this paper (https://arxiv.org/abs/1506.01186).
    The amplitude of the cycle can be scaled on a per-iteration or
    per-cycle basis.
    This class has three built-in policies, as put forth in the paper.
    "triangular":
        A basic triangular cycle w/ no amplitude scaling.
    "triangular2":
        A basic triangular cycle that scales initial amplitude by half each cycle.
    "exp_range":
        A cycle that scales initial amplitude by gamma**(cycle iterations) at each
        cycle iteration.
    For more detail, please see paper.

    # Example
        ```python
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., mode='triangular')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```

    Class also supports custom scaling functions:
        ```python
            clr_fn = lambda x: 0.5*(1+np.sin(x*np.pi/2.))
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., scale_fn=clr_fn,
                                scale_mode='cycle')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```
    # Arguments
        base_lr: initial learning rate which is the
            lower boundary in the cycle.
        max_lr: upper boundary in the cycle. Functionally,
            it defines the cycle amplitude (max_lr - base_lr).
            The lr at any cycle is the sum of base_lr
            and some scaling of the amplitude; therefore
            max_lr may not actually be reached depending on
            scaling function.
        step_size: number of training iterations per
            half cycle. Authors suggest setting step_size
            2-8 x training iterations in epoch.
        mode: one of {triangular, triangular2, exp_range}.
            Default 'triangular'.
            Values correspond to policies detailed above.
            If scale_fn is not None, this argument is ignored.
        gamma: constant in 'exp_range' scaling function:
            gamma**(cycle iterations)
        scale_fn: Custom scaling policy defined by a single
            argument lambda function, where
            0 <= scale_fn(x) <= 1 for all x >= 0.
            mode paramater is ignored
        scale_mode: {'cycle', 'iterations'}.
            Defines whether scale_fn is evaluated on
            cycle number or cycle iterations (training
            iterations since start of cycle). Default is 'cycle'.
    """

    def __init__(self, base_lr=0.001, max_lr=0.006, step_size=2000., mode='triangular',
                 gamma=1., scale_fn=None, scale_mode='cycle'):
        super(CyclicLR, self).__init__()

        self.base_lr = base_lr
        self.max_lr = max_lr
        self.step_size = step_size
        self.mode = mode
        self.gamma = gamma
        if scale_fn == None:
            if self.mode == 'triangular':
                self.scale_fn = lambda x: 1.
                self.scale_mode = 'cycle'
            elif self.mode == 'triangular2':
                self.scale_fn = lambda x: 1/(2.**(x-1))
                self.scale_mode = 'cycle'
            elif self.mode == 'exp_range':
                self.scale_fn = lambda x: gamma**(x)
                self.scale_mode = 'iterations'
        else:
            self.scale_fn = scale_fn
            self.scale_mode = scale_mode
        self.clr_iterations = 0.
        self.trn_iterations = 0.
        self.history = {}

        self._reset()

    def _reset(self, new_base_lr=None, new_max_lr=None,
               new_step_size=None):
        """Resets cycle iterations.
        Optional boundary/step size adjustment.
        """
        if new_base_lr != None:
            self.base_lr = new_base_lr
        if new_max_lr != None:
            self.max_lr = new_max_lr
        if new_step_size != None:
            self.step_size = new_step_size
        self.clr_iterations = 0.

    def clr(self):
        cycle = np.floor(1+self.clr_iterations/(2*self.step_size))
        x = np.abs(self.clr_iterations/self.step_size - 2*cycle + 1)
        if self.scale_mode == 'cycle':
            return self.base_lr + (self.max_lr-self.base_lr)*np.maximum(0, (1-x))*self.scale_fn(cycle)
        else:
            return self.base_lr + (self.max_lr-self.base_lr)*np.maximum(0, (1-x))*self.scale_fn(self.clr_iterations)

    def on_train_begin(self, logs={}):
        logs = logs or {}

        if self.clr_iterations == 0:
            if hasattr(self.model.optimizer, 'lr'):
                K.set_value(self.model.optimizer.lr, self.base_lr)
            else:
                K.set_value(self.model.optimizer.optimizer._lr, self.base_lr)
        else:
            if hasattr(self.model.optimizer, 'lr'):
                K.set_value(self.model.optimizer.lr, self.clr())
            else:
                K.set_value(self.model.optimizer.optimizer._lr, self.clr())

    def on_batch_end(self, epoch, logs=None):

        logs = logs or {}
        self.trn_iterations += 1
        self.clr_iterations += 1

        if hasattr(self.model.optimizer, 'lr'):
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
        else:
            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.optimizer._lr))

        self.history.setdefault('iterations', []).append(self.trn_iterations)

        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

        if hasattr(self.model.optimizer, 'lr'):
            K.set_value(self.model.optimizer.lr, self.clr())
        else:
            K.set_value(self.model.optimizer.optimizer._lr, self.clr())
